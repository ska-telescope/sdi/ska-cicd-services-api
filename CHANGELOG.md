CHANGELOG
=========

### **Release Notes Information**
- This `CHANGELOG.md` file was added starting with version 1.0.0. You can find the previous release's notes in the [Release Notes](https://gitlab.com/ska-telescope/sdi/ska-cicd-services-api/-/releases).

## 1.0.0

**Breaking:** Changed ReadTheDocsApi to use httpx instead of aiohttp ([ST-2211](https://jira.skatelescope.org/browse/ST-2211)).

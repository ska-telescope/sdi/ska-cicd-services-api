# pylint: disable=redefined-outer-name
from unittest.mock import MagicMock, patch

import pytest

from ska_cicd_services_api.nexus_api import NexusApi


def create_mock_coroutine(data):
    async def mock_coroutine():
        return data

    return mock_coroutine


@pytest.fixture
def mock_aiohttp_session():
    return MagicMock()


@pytest.fixture
def nexus_api(mock_aiohttp_session):
    with patch("aiohttp.ClientSession", return_value=mock_aiohttp_session):
        yield NexusApi(session=mock_aiohttp_session)


@pytest.mark.asyncio
async def test_get_components_list(nexus_api, mock_aiohttp_session):
    mock_response = MagicMock()
    mock_response.status = 200
    mock_response.json = create_mock_coroutine(
        {"continuationToken": None, "items": [{"name": "component1"}]}
    )
    mock_aiohttp_session.get.return_value.__aenter__.return_value = (
        mock_response
    )

    components = [
        component
        async for component in await nexus_api.get_components_list(
            "repository"
        )
    ]
    assert len(components) == 1
    assert components[0]["name"] == "component1"


@pytest.mark.asyncio
async def test_get_component_info(nexus_api, mock_aiohttp_session):
    mock_response = MagicMock()
    mock_response.status = 200
    mock_response.json = create_mock_coroutine({"name": "component1"})
    mock_aiohttp_session.request.return_value.__aenter__.return_value = (
        mock_response
    )

    component_info = await nexus_api.get_component_info("component_id")
    assert component_info["name"] == "component1"


@pytest.mark.asyncio
async def test_remove_component(nexus_api, mock_aiohttp_session):
    mock_response = MagicMock()
    mock_response.status = 200
    mock_response.json = create_mock_coroutine({"result": "successfull"})
    mock_aiohttp_session.request.return_value.__aenter__.return_value = (
        mock_response
    )

    result = await nexus_api.remove_component("component_id")
    assert result == {"result": "successfull"}


@pytest.mark.asyncio
async def test_upload_component(nexus_api, mock_aiohttp_session):
    mock_response = MagicMock()
    mock_response.status = 200
    mock_response.json = create_mock_coroutine({"result": "successfull"})
    mock_aiohttp_session.request.return_value.__aenter__.return_value = (
        mock_response
    )

    result = await nexus_api.upload_component(
        "maven",
        "repository",
        maven2_group_id="group",
        maven2_artifact_id="artifact",
    )
    assert result == {"result": "successfull"}

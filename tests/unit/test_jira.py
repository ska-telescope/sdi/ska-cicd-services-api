# pylint: disable=redefined-outer-name
from unittest.mock import AsyncMock, MagicMock, patch

import pytest
from jira.resources import Comment

from ska_cicd_services_api.jira_api import JiraApi


async def jira_api():
    api = JiraApi()
    with patch("jira.JIRA.__init__", return_value=None):
        await api.authenticate("test", "test", "test")
    return api


@pytest.mark.asyncio
async def test_get_projects():
    mock_projects = [
        {"key": "key1", "id": 1, "name": "name1", "type": "type1"}
    ]
    mock_return = [MagicMock(**item) for item in mock_projects]

    with patch(
        "jira.JIRA.projects", new_callable=AsyncMock
    ) as mock_async_function:
        mock_async_function.return_value = mock_return

        api = await jira_api()
        result = await api.get_projects()

        assert len(result) == 1
        assert result[0]["key"] == mock_return[0].key
        assert result[0]["id"] == mock_return[0].id
        assert result[0]["name"] == mock_return[0].name
        assert result[0]["type"] == mock_return[0].projectTypeKey


@pytest.mark.asyncio
async def test_get_issue():
    with patch(
        "jira.JIRA.issue", new_callable=AsyncMock
    ) as mock_async_function:
        mock_async_function.return_value = {
            "key": "ST-443",
            "summary": "Test Issue",
        }

        api = await jira_api()
        result = await api.get_issue(issue_key="ST-443")

        assert result == {"key": "ST-443", "summary": "Test Issue"}


@pytest.mark.asyncio
async def test_create_issues():
    with patch(
        "jira.JIRA.create_issues", new_callable=AsyncMock
    ) as mock_async_function:
        mock_async_function.return_value = [
            {"key": "KEY1", "summary": "Summary1"},
            {"key": "KEY2", "summary": "Summary2"},
        ]

        issue_list = [
            {
                "project": {"key": "CHR"},
                "summary": "Summary1",
                "issuetype": {"name": "Type1"},
            },
            {
                "project": {"key": "CHR"},
                "summary": "Summary2",
                "issuetype": {"name": "Type2"},
            },
        ]

        api = await jira_api()
        result = await api.create_issues(issue_list=issue_list)

        assert result == [
            {"key": "KEY1", "summary": "Summary1"},
            {"key": "KEY2", "summary": "Summary2"},
        ]


@pytest.mark.asyncio
async def test_comment_on_issue():
    with patch(
        "jira.JIRA.add_comment", new_callable=AsyncMock
    ) as mock_async_function:
        mock_issue = {"key": "ST-443", "summary": "Test Issue"}
        mock_async_function.return_value = {"comment": "New comment"}

        api = await jira_api()
        result = await api.comment_on_issue(
            issue=mock_issue, comment="New comment"
        )

        assert result == {"comment": "New comment"}


@pytest.mark.asyncio
async def test_delete_comment():
    mock_comment = MagicMock(spec=Comment)
    api = await jira_api()
    await api.delete_comment(comment=mock_comment)

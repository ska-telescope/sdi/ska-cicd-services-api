import time
from unittest.mock import MagicMock, patch

import pytest

from ska_cicd_services_api.people_database_api import (
    PeopleDatabaseApi,
    PeopleDatabaseColumnMapping,
    PeopleDatabaseField,
    PeopleDatabaseUser,
)

# Mock sheet content for testing. Less columns are returned
# by the Google Spreadsheet API when they are empty
mock_sheet_content = [
    ["name1", "", "team1", "slack1", "gitlab1"],
    ["name2", "email2", "team2", "", "gitlab2"],
    ["name3", "email3", "team3", "slack3"],
]

mock_database_mapping = PeopleDatabaseColumnMapping()

# Mock service account data
service_account_data = {
    "type": "service_account",
    "project_id": "test_project_id",
    "private_key_id": "test_private_key_id",
    "private_key": "test_private_key",
    "client_email": "test_client_email",
    "client_id": "test_client_id",
    "auth_uri": "https://accounts.google.com/o/oauth2/auth",
    "token_uri": "https://oauth2.googleapis.com/token",
    "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",  # NOQA: E501
    "client_x509_cert_url": "test_client_x509_cert_url",
}


@pytest.mark.asyncio
@patch(
    "ska_cicd_services_api.people_database_api.service_account.Credentials.from_service_account_info",  # NOQA: E501
    autospec=True,
)
async def test_get_sheet(mock_creds):
    # pylint: disable=W0613
    with patch(
        "ska_cicd_services_api.people_database_api.discovery.build"
    ) as mock_build:
        mock_service = MagicMock()
        mock_service.spreadsheets().values().get().execute.return_value = {
            "values": mock_sheet_content
        }
        mock_build.return_value = mock_service

        api = PeopleDatabaseApi(
            service_account_data,
            spreadsheet_id="test_sheet_id",
            spreadsheet_range="test_sheet_range",
            column_mapping=mock_database_mapping,
        )
        sheet_content = (
            await api._get_sheet()  # pylint: disable=protected-access
        )

        assert sheet_content == mock_sheet_content
        assert "sheet_content" in api.cache
        assert api.cache["sheet_content"] == mock_sheet_content


@pytest.mark.asyncio
@patch(
    "ska_cicd_services_api.people_database_api.service_account.Credentials.from_service_account_info",  # NOQA: E501
    autospec=True,
)
async def test_get_sheet_cache_reload(mock_creds):
    # pylint: disable=W0613
    with patch(
        "ska_cicd_services_api.people_database_api.discovery.build"
    ) as mock_build:
        mock_service = MagicMock()
        mock_service.spreadsheets().values().get().execute.return_value = {
            "values": mock_sheet_content
        }
        mock_build.return_value = mock_service

        api = PeopleDatabaseApi(
            service_account_data,
            cache_ttl=0.1,
            spreadsheet_id="test_sheet_id",
            spreadsheet_range="test_sheet_range",
            column_mapping=mock_database_mapping,
        )

        sheet_content_1 = (
            await api._get_sheet(  # pylint: disable=protected-access
                "slack_id"
            )
        )

        # One of the users is missing slack_id on purpose
        assert (
            len(sheet_content_1.keys()) == len(api.cache["sheet_content"]) - 1
        )

        time.sleep(0.1)
        assert not api._cache_available()  # pylint: disable=protected-access

        sheet_content_2 = (
            await api._get_sheet(  # pylint: disable=protected-access
                "slack_id"
            )
        )
        assert sheet_content_2.keys() == sheet_content_1.keys()


@pytest.mark.asyncio
@patch(
    "ska_cicd_services_api.people_database_api.service_account.Credentials.from_service_account_info",  # NOQA: E501
    autospec=True,
)
async def test_get_user_by_email(mock_creds):
    # pylint: disable=W0613
    with patch(
        "ska_cicd_services_api.people_database_api.discovery.build"
    ) as mock_build:
        mock_service = MagicMock()
        mock_service.spreadsheets().values().get().execute.return_value = {
            "values": mock_sheet_content
        }
        mock_build.return_value = mock_service

        api = PeopleDatabaseApi(
            service_account_data,
            spreadsheet_id="test_sheet_id",
            spreadsheet_range="test_sheet_range",
            column_mapping=mock_database_mapping,
        )
        user1 = await api.get_user_by_email("email1")
        user2 = await api.get_user_by_email("email2")
        user3 = await api.get_user_by_email("email3")

        assert not user1

        assert user2
        assert user2.name == "name2"
        assert user2.email == "email2"
        assert user2.team == "team2"
        assert user2.slack_id == ""
        assert user2.gitlab_handle == "gitlab2"

        assert user3
        assert user3.name == "name3"
        assert user3.email == "email3"
        assert user3.team == "team3"
        assert user3.slack_id == "slack3"
        assert user3.gitlab_handle == ""


@pytest.mark.asyncio
@patch(
    "ska_cicd_services_api.people_database_api.service_account.Credentials.from_service_account_info",  # NOQA: E501
    autospec=True,
)
async def test_get_user_by_slack_id(mock_creds):
    # pylint: disable=W0613
    with patch(
        "ska_cicd_services_api.people_database_api.discovery.build"
    ) as mock_build:
        mock_service = MagicMock()
        mock_service.spreadsheets().values().get().execute.return_value = {
            "values": mock_sheet_content
        }
        mock_build.return_value = mock_service

        api = PeopleDatabaseApi(
            service_account_data,
            spreadsheet_id="test_sheet_id",
            spreadsheet_range="test_sheet_range",
            column_mapping=mock_database_mapping,
        )
        user1 = await api.get_user_by_slack_id("slack1")
        user2 = await api.get_user_by_slack_id("slack2")
        user3 = await api.get_user_by_slack_id("slack3")

        assert user1
        assert user1.name == "name1"
        assert user1.email == ""
        assert user1.team == "team1"
        assert user1.slack_id == "slack1"
        assert user1.gitlab_handle == "gitlab1"

        assert not user2

        assert user3
        assert user3.name == "name3"
        assert user3.email == "email3"
        assert user3.team == "team3"
        assert user3.slack_id == "slack3"
        assert user3.gitlab_handle == ""


@pytest.mark.asyncio
@patch(
    "ska_cicd_services_api.people_database_api.service_account.Credentials.from_service_account_info",  # NOQA: E501
    autospec=True,
)
async def test_get_user_by_gitlab_handle(mock_creds):
    # pylint: disable=W0613
    with patch(
        "ska_cicd_services_api.people_database_api.discovery.build"
    ) as mock_build:
        mock_service = MagicMock()
        mock_service.spreadsheets().values().get().execute.return_value = {
            "values": mock_sheet_content
        }
        mock_build.return_value = mock_service

        api = PeopleDatabaseApi(
            service_account_data,
            spreadsheet_id="test_sheet_id",
            spreadsheet_range="test_sheet_range",
            column_mapping=mock_database_mapping,
        )
        user1 = await api.get_user_by_gitlab_handle("gitlab1")
        user2 = await api.get_user_by_gitlab_handle("gitlab2")
        user3 = await api.get_user_by_gitlab_handle("gitlab3")

        assert user1
        assert user1.name == "name1"
        assert user1.email == ""
        assert user1.team == "team1"
        assert user1.slack_id == "slack1"
        assert user1.gitlab_handle == "gitlab1"

        assert user2
        assert user2.name == "name2"
        assert user2.email == "email2"
        assert user2.team == "team2"
        assert user2.slack_id == ""
        assert user2.gitlab_handle == "gitlab2"

        assert not user3


class ExtendedPeopleDatabaseColumnMapping(PeopleDatabaseColumnMapping):
    country: PeopleDatabaseField = PeopleDatabaseField(column=6)


class ExtendedPeopleDatabaseUser(PeopleDatabaseUser):
    country: str


@pytest.mark.asyncio
@patch(
    "ska_cicd_services_api.people_database_api.service_account.Credentials.from_service_account_info",  # NOQA: E501
    autospec=True,
)
async def test_extensibility(mock_creds):
    # pylint: disable=W0613
    with patch(
        "ska_cicd_services_api.people_database_api.discovery.build"
    ) as mock_build:
        extended_mock_sheet_content = [
            ["name1", "", "team1", "slack1", "gitlab1", "country1"],
            ["name2", "email2", "team2", "", "gitlab2", ""],
            ["name3", "email3", "team3", "slack3", "", "country3"],
        ]

        extended_mock_database_mapping = ExtendedPeopleDatabaseColumnMapping()
        mock_service = MagicMock()
        mock_service.spreadsheets().values().get().execute.return_value = {
            "values": extended_mock_sheet_content
        }
        mock_build.return_value = mock_service

        api = PeopleDatabaseApi(
            service_account_data,
            spreadsheet_id="test_sheet_id",
            spreadsheet_range="test_sheet_range",
            column_mapping=extended_mock_database_mapping,
            user_class=ExtendedPeopleDatabaseUser,
        )
        user1: ExtendedPeopleDatabaseUser = (
            await api.get_user_by_gitlab_handle("gitlab1")
        )
        user2: ExtendedPeopleDatabaseUser = (
            await api.get_user_by_gitlab_handle("gitlab2")
        )
        user3: ExtendedPeopleDatabaseUser = (
            await api.get_user_by_gitlab_handle("gitlab3")
        )

        assert user1
        assert user1.country == "country1"

        assert user2
        assert user2.country == ""

        assert user3 is None

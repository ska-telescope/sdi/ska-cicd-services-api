# pylint: disable=redefined-outer-name
from unittest.mock import AsyncMock, patch

import pytest
from harborapi.models import (
    Artifact,
    HarborVulnerabilityReport,
    Label,
    Repository,
    Tag,
)

from ska_cicd_services_api.harbor_api import HarborApi

api = HarborApi("test", "test", "test")


@pytest.mark.asyncio
async def test_get_repository():
    repo_data = {"name": "test_repo"}
    mock_return = Repository(**repo_data)

    with patch(
        "src.ska_cicd_services_api.harbor_api.HarborAsyncClient.get_repository",  # NOQA: E501
        new_callable=AsyncMock,
    ) as mock_async_function:
        mock_async_function.return_value = mock_return
        result = await api.get_repository("project", "repository")

        assert result.name == "test_repo"


@pytest.mark.asyncio
async def test_get_repositories():
    repos_data = [{"name": "repo1"}, {"name": "repo2"}]
    mock_return = [Repository(**data) for data in repos_data]

    with patch(
        "src.ska_cicd_services_api.harbor_api.HarborAsyncClient.get_repositories",  # NOQA: E501
        new_callable=AsyncMock,
    ) as mock_async_function:
        mock_async_function.return_value = mock_return
        result = await api.get_repositories("project")

        assert len(result) == 2
        assert result[0].name == "repo1"
        assert result[1].name == "repo2"


@pytest.mark.asyncio
async def test_delete_repository():
    with patch(
        "src.ska_cicd_services_api.harbor_api.HarborAsyncClient.delete_repository",  # NOQA: E501
        new_callable=AsyncMock,
    ) as mock_async_function:
        await api.delete_repository("project", "repository")

        mock_async_function.assert_called_once_with("project", "repository")


@pytest.mark.asyncio
async def test_delete_artifact():
    with patch(
        "src.ska_cicd_services_api.harbor_api.HarborAsyncClient.delete_artifact",  # NOQA: E501
        new_callable=AsyncMock,
    ) as mock_async_function:
        await api.delete_artifact("project", "repository", "reference")

        mock_async_function.assert_called_once_with(
            "project", "repository", "reference"
        )


@pytest.mark.asyncio
async def test_get_artifact():
    artifact_data = {"name": "test_artifact"}
    mock_return = Artifact(**artifact_data)

    with patch(
        "src.ska_cicd_services_api.harbor_api.HarborAsyncClient.get_artifact",
        new_callable=AsyncMock,
    ) as mock_async_function:
        mock_async_function.return_value = mock_return
        result = await api.get_artifact("project", "repository", "reference")
        assert result.name == "test_artifact"

        mock_async_function.assert_called_once_with(
            "project",
            "repository",
            "reference",
            page=1,
            page_size=20,
            with_tag=True,
            with_label=True,
        )


@pytest.mark.asyncio
async def test_copy_artifact():
    mock_return = "/path/to/new_artifact"

    with patch(
        "src.ska_cicd_services_api.harbor_api.HarborAsyncClient.copy_artifact",
        new_callable=AsyncMock,
    ) as mock_async_function:
        mock_async_function.return_value = mock_return
        result = await api.copy_artifact(
            "new_project", "new_repository", "source_artifact"
        )

        mock_async_function.assert_called_once_with(
            "new_project", "new_repository", "source_artifact"
        )
        assert result == "/path/to/new_artifact"


@pytest.mark.asyncio
async def test_get_artifact_vulnerabilities():
    vulnerabilities_data = {
        "vulnerability_1": "critical",
        "vulnerability_2": "high",
    }
    mock_return = HarborVulnerabilityReport(**vulnerabilities_data)

    with patch(
        "src.ska_cicd_services_api.harbor_api.HarborAsyncClient.get_artifact_vulnerabilities",  # NOQA: E501
        new_callable=AsyncMock,
    ) as mock_async_function:
        mock_async_function.return_value = mock_return
        result = await api.get_artifact_vulnerabilities(
            "project", "repository", "reference"
        )

        assert result == mock_return

        mock_async_function.assert_called_once_with(
            "project",
            "repository",
            "reference",
        )


@pytest.mark.asyncio
async def test_get_artifact_tags():
    tags_data = [{"name": "tag1"}, {"name": "tag2"}]
    mock_return = [Tag(**data) for data in tags_data]

    with patch(
        "src.ska_cicd_services_api.harbor_api.HarborAsyncClient.get_artifact_tags",  # NOQA: E501
        new_callable=AsyncMock,
    ) as mock_async_function:
        mock_async_function.return_value = mock_return
        result = await api.get_artifact_tags(
            "project", "repository", "reference", page=1, page_size=10
        )

        assert len(result) == 2
        assert result[0].name == "tag1"
        assert result[1].name == "tag2"

        mock_async_function.assert_called_once_with(
            "project",
            "repository",
            "reference",
            page=1,
            page_size=10,
            with_signature=False,
            with_immutable_status=False,
        )


@pytest.mark.asyncio
async def test_create_artifact_tag():
    mock_return = "/path/to/created_tag"

    with patch(
        "src.ska_cicd_services_api.harbor_api.HarborAsyncClient.create_artifact_tag",  # NOQA: E501
        new_callable=AsyncMock,
    ) as mock_async_function:
        mock_async_function.return_value = mock_return
        tag = Tag(name="test_tag")
        result = await api.create_artifact_tag(
            "project", "repository", "reference", tag
        )

        # Assert that the method was called with the correct arguments
        mock_async_function.assert_called_once_with(
            "project", "repository", "reference", tag
        )
        assert result == "/path/to/created_tag"


@pytest.mark.asyncio
async def test_delete_artifact_tag():
    with patch(
        "src.ska_cicd_services_api.harbor_api.HarborAsyncClient.delete_artifact_tag",  # NOQA: E501
        new_callable=AsyncMock,
    ) as mock_async_function:
        await api.delete_artifact_tag(
            "project", "repository", "reference", "test_tag"
        )

        mock_async_function.assert_called_once_with(
            "project", "repository", "reference", "test_tag"
        )


@pytest.mark.asyncio
async def test_get_labels():
    labels_data = [{"name": "label1"}, {"name": "label2"}]
    mock_return = [Label(**data) for data in labels_data]

    with patch(
        "src.ska_cicd_services_api.harbor_api.HarborAsyncClient.get_labels",
        new_callable=AsyncMock,
    ) as mock_async_function:
        mock_async_function.return_value = mock_return
        result = await api.get_labels(name="test_label", scope="g")

        assert len(result) == 2
        assert result[0].name == "label1"
        assert result[1].name == "label2"

        mock_async_function.assert_called_once_with(
            name="test_label", scope="g"
        )


@pytest.mark.asyncio
async def test_create_label():
    with patch(
        "src.ska_cicd_services_api.harbor_api.HarborAsyncClient.create_label",
        new_callable=AsyncMock,
    ) as mock_async_function:
        label = "test_label"
        scope = "g"
        await api.create_label(label, scope)

        label = Label(name=label, scope=scope)
        mock_async_function.assert_called_once_with(label)


@pytest.mark.asyncio
async def test_add_artifact_label():
    with patch(
        "src.ska_cicd_services_api.harbor_api.HarborAsyncClient.add_artifact_label",  # NOQA: E501
        new_callable=AsyncMock,
    ) as mock_async_function:
        label = Label(name="test_label")
        await api.add_artifact_label(
            "project", "repository", "reference", label
        )

        mock_async_function.assert_called_once_with(
            "project", "repository", "reference", label
        )


@pytest.mark.asyncio
async def test_delete_artifact_label():
    with patch(
        "src.ska_cicd_services_api.harbor_api.HarborAsyncClient.delete_artifact_label",  # NOQA: E501
        new_callable=AsyncMock,
    ) as mock_async_function:
        await api.delete_artifact_label(
            "project", "repository", "reference", label_id=1
        )

        mock_async_function.assert_called_once_with(
            "project", "repository", "reference", 1
        )

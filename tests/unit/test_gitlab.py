# pylint: disable=redefined-outer-name
from unittest.mock import AsyncMock, MagicMock, patch

import aiohttp
import pytest

from ska_cicd_services_api.gitlab_api import (
    GitLabApi,  # Import the actual module containing GitLabApi
)


async def create_async_generator(items):
    for item in items:
        yield item


@pytest.fixture
def mock_session():
    return MagicMock(spec=aiohttp.ClientSession)


@pytest.mark.asyncio
async def test_get_current_user(mock_session):
    with patch(
        "gidgetlab.aiohttp.GitLabAPI.getitem", new_callable=AsyncMock
    ) as mock_async_function:
        mock_async_function.return_value = {"username": "test_user"}

        api = GitLabApi(mock_session)
        result = await api.get_current_user()

        assert result == {"username": "test_user"}


@pytest.mark.asyncio
async def test_get_commit_diffs(mock_session):
    with patch(
        "gidgetlab.aiohttp.GitLabAPI.getiter", new_callable=MagicMock
    ) as mock_async_function:
        mock_async_function.return_value = ["diff1", "diff2"]

        api = GitLabApi(mock_session)
        result = await api.get_commit_diffs("123", "abc123")

        assert result == ["diff1", "diff2"]


@pytest.mark.asyncio
async def test_add_comment(mock_session):
    with patch(
        "gidgetlab.aiohttp.GitLabAPI.post", new_callable=AsyncMock
    ) as mock_async_function:
        mock_async_function.return_value = {"comment": "added"}

        api = GitLabApi(mock_session)
        result = await api.add_comment(1, 2, "Nice work!")
        assert result == {"comment": "added"}


@pytest.mark.asyncio
async def test_remove_comment(mock_session):
    with patch(
        "gidgetlab.aiohttp.GitLabAPI.delete", new_callable=AsyncMock
    ) as mock_async_function:
        mock_async_function.return_value = {"status": "success"}

        api = GitLabApi(mock_session)
        result = await api.remove_comment(1, 2, 3)
        assert result == {"status": "success"}


@pytest.mark.asyncio
async def test_get_merge_request_info(mock_session):
    with patch(
        "gidgetlab.aiohttp.GitLabAPI.getitem", new_callable=AsyncMock
    ) as mock_async_function:
        mock_async_function.return_value = {"title": "Test MR"}

        api = GitLabApi(mock_session)
        result = await api.get_merge_request_info(1, 2)
        assert result == {"title": "Test MR"}


@pytest.mark.asyncio
async def test_get_merge_request_approvals_info(mock_session):
    with patch(
        "gidgetlab.aiohttp.GitLabAPI.getitem", new_callable=AsyncMock
    ) as mock_async_function:
        mock_async_function.return_value = {"approvals": 3}

        api = GitLabApi(mock_session)
        result = await api.get_merge_request_approvals_info(1, 2)
        assert result == {"approvals": 3}


@pytest.mark.asyncio
async def test_add_approval_merge_request(mock_session):
    with patch(
        "gidgetlab.aiohttp.GitLabAPI.post", new_callable=AsyncMock
    ) as mock_async_function:
        mock_async_function.return_value = {"status": "approved"}

        api = GitLabApi(mock_session)
        result = await api.add_approval_merge_request(1, 2)
        assert result == {"status": "approved"}


@pytest.mark.asyncio
async def test_remove_approval_merge_request(mock_session):
    with patch(
        "gidgetlab.aiohttp.GitLabAPI.post", new_callable=AsyncMock
    ) as mock_async_function:
        mock_async_function.return_value = {"status": "unapproved"}

        api = GitLabApi(mock_session)
        result = await api.remove_approval_merge_request(1, 2)
        assert result == {"status": "unapproved"}


@pytest.mark.asyncio
async def test_get_project_merge_request_approvals_info(mock_session):
    with patch(
        "gidgetlab.aiohttp.GitLabAPI.getitem", new_callable=AsyncMock
    ) as mock_async_function:
        mock_async_function.return_value = {"approvals": 5}

        api = GitLabApi(mock_session)
        result = await api.get_project_merge_request_approvals_info(1)
        assert result == {"approvals": 5}


@pytest.mark.asyncio
async def test_get_merge_request_commits(mock_session):
    with patch(
        "gidgetlab.aiohttp.GitLabAPI.getiter", new_callable=MagicMock
    ) as mock_async_function:
        mock_async_function.return_value = ["commit1", "commit2"]

        api = GitLabApi(mock_session)
        result = await api.get_merge_request_commits(1, 2)
        assert result == ["commit1", "commit2"]


@pytest.mark.asyncio
async def test_get_project_settings_info(mock_session):
    with patch(
        "gidgetlab.aiohttp.GitLabAPI.getitem", new_callable=AsyncMock
    ) as mock_async_function:
        mock_async_function.return_value = {"project_name": "Test Project"}

        api = GitLabApi(mock_session)
        result = await api.get_project_settings_info(1)
        assert result == {"project_name": "Test Project"}


@pytest.mark.asyncio
async def test_get_project_members(mock_session):
    with patch(
        "gidgetlab.aiohttp.GitLabAPI.getiter", new_callable=MagicMock
    ) as mock_async_function:
        mock_async_function.return_value = ["user1", "user2"]

        api = GitLabApi(mock_session)
        result = await api.get_project_members(1)
        assert result == ["user1", "user2"]


@pytest.mark.asyncio
async def test_get_merge_request_comments(mock_session):
    with patch(
        "gidgetlab.aiohttp.GitLabAPI.getiter", new_callable=MagicMock
    ) as mock_async_function:
        mock_async_function.return_value = ["comment1", "comment2"]

        api = GitLabApi(mock_session)
        result = await api.get_merge_request_comments(1, 2)
        assert result == ["comment1", "comment2"]


@pytest.mark.asyncio
async def test_get_comment_info(mock_session):
    with patch(
        "gidgetlab.aiohttp.GitLabAPI.getitem", new_callable=AsyncMock
    ) as mock_async_function:
        mock_async_function.return_value = {"comment": "info"}

        api = GitLabApi(mock_session)
        result = await api.get_comment_info(1, 2, 3)
        assert result == {"comment": "info"}


@pytest.mark.asyncio
async def test_get_job_token_scope(mock_session):
    with patch(
        "gidgetlab.aiohttp.GitLabAPI.getitem", new_callable=AsyncMock
    ) as mock_async_function:
        mock_async_function.return_value = [
            "inbound_enabled",
            "outbound_enabled",
        ]

        api = GitLabApi(mock_session)
        result = await api.get_job_token_scope(1)
        assert result == ["inbound_enabled", "outbound_enabled"]


@pytest.mark.asyncio
async def test_modify_comment(mock_session):
    with patch(
        "gidgetlab.aiohttp.GitLabAPI.put", new_callable=AsyncMock
    ) as mock_async_function:
        mock_async_function.return_value = {"status": "modified"}

        api = GitLabApi(mock_session)
        result = await api.modify_comment(1, 2, 3, "Updated comment")
        assert result == {"status": "modified"}


@pytest.mark.asyncio
async def test_modify_merge_request(mock_session):
    with patch(
        "gidgetlab.aiohttp.GitLabAPI.put", new_callable=AsyncMock
    ) as mock_async_function:
        mock_async_function.return_value = {"status": "modified"}

        api = GitLabApi(mock_session)
        result = await api.modify_merge_request(
            1, 2, title="New Title", labels=["bug"]
        )
        assert result == {"status": "modified"}


@pytest.mark.asyncio
async def test_modify_project_level_mr_approval_settings(mock_session):
    with patch(
        "gidgetlab.aiohttp.GitLabAPI.post", new_callable=AsyncMock
    ) as mock_async_function:
        mock_async_function.return_value = {"status": "modified"}

        api = GitLabApi(mock_session)
        result = await api.modify_project_level_mr_approval_settings(
            1, approvals_before_merge=2
        )
        assert result == {"status": "modified"}


@pytest.mark.asyncio
async def test_modify_mr_level_mr_approval_settings(mock_session):
    with patch(
        "gidgetlab.aiohttp.GitLabAPI.post", new_callable=AsyncMock
    ) as mock_async_function:
        mock_async_function.return_value = {"status": "modified"}

        api = GitLabApi(mock_session)
        result = await api.modify_mr_level_mr_approval_settings(
            1, 2, approvals_required=3
        )
        assert result == {"status": "modified"}


@pytest.mark.asyncio
async def test_modify_project(mock_session):
    with patch(
        "gidgetlab.aiohttp.GitLabAPI.put", new_callable=AsyncMock
    ) as mock_async_function:
        mock_async_function.return_value = {"status": "modified"}

        api = GitLabApi(mock_session)
        result = await api.modify_project(
            1, description="New Description", visibility="public"
        )
        assert result == {"status": "modified"}


@pytest.mark.asyncio
async def test_get_search_results(mock_session):
    with patch(
        "gidgetlab.aiohttp.GitLabAPI.getiter", new_callable=MagicMock
    ) as mock_async_function:
        mock_async_function.return_value = ["result1", "result2"]

        api = GitLabApi(mock_session)
        result = await api.get_search_results(1, "issues", "test")
        assert result == ["result1", "result2"]


@pytest.mark.asyncio
async def test_get_file_from_repository(mock_session):
    with patch(
        "gidgetlab.aiohttp.GitLabAPI.getitem", new_callable=AsyncMock
    ) as mock_async_function:
        mock_async_function.return_value = {"file_content": "content"}

        api = GitLabApi(mock_session)
        result = await api.get_file_from_repository(1, "file_path", "master")
        assert result == {"file_content": "content"}


@pytest.mark.asyncio
async def test_get_repo_tree(mock_session):
    with patch(
        "gidgetlab.aiohttp.GitLabAPI.getiter", new_callable=MagicMock
    ) as mock_async_function:
        mock_async_function.return_value = ["file1", "file2"]

        api = GitLabApi(mock_session)
        result = await api.get_repo_tree(1, ref="master", path="/")
        assert result == ["file1", "file2"]


@pytest.mark.asyncio
async def test_get_mr_diff_versions(mock_session):
    with patch(
        "gidgetlab.aiohttp.GitLabAPI.getiter", new_callable=MagicMock
    ) as mock_async_function:
        mock_async_function.return_value = ["version1", "version2"]

        api = GitLabApi(mock_session)
        result = await api.get_mr_diff_versions(1, 2)
        assert result == ["version1", "version2"]


@pytest.mark.asyncio
async def test_get_single_single_mr_diff_version(mock_session):
    with patch(
        "gidgetlab.aiohttp.GitLabAPI.getitem", new_callable=AsyncMock
    ) as mock_async_function:
        mock_async_function.return_value = {"version_data": "data"}

        api = GitLabApi(mock_session)
        result = await api.get_single_single_mr_diff_version(1, 2, 3)
        assert result == {"version_data": "data"}


@pytest.mark.asyncio
async def test_get_project_hooks(mock_session):
    with patch(
        "gidgetlab.aiohttp.GitLabAPI.getiter", new_callable=MagicMock
    ) as mock_async_function:
        mock_async_function.return_value = ["hook1", "hook2"]

        api = GitLabApi(mock_session)
        result = await api.get_project_hooks(1)
        assert result == ["hook1", "hook2"]


@pytest.mark.asyncio
async def test_create_mr(mock_session):
    with patch(
        "gidgetlab.aiohttp.GitLabAPI.post", new_callable=AsyncMock
    ) as mock_async_function:
        mock_async_function.return_value = {"mr_data": "created"}

        api = GitLabApi(mock_session)
        result = await api.create_mr(
            1, "source_branch", "target_branch", "Title", assignee_id=2
        )
        assert result == {"mr_data": "created"}


@pytest.mark.asyncio
async def test_create_branch(mock_session):
    with patch(
        "gidgetlab.aiohttp.GitLabAPI.post", new_callable=AsyncMock
    ) as mock_async_function:
        mock_async_function.return_value = {"branch_data": "created"}

        api = GitLabApi(mock_session)
        result = await api.create_branch(1, "new_branch", ref="master")
        assert result == {"branch_data": "created"}


@pytest.mark.asyncio
async def test_get_merge_requests(mock_session):
    with patch(
        "gidgetlab.aiohttp.GitLabAPI.getiter", new_callable=MagicMock
    ) as mock_async_function:
        mock_async_function.return_value = ["mr1", "mr2"]

        api = GitLabApi(mock_session)
        result = await api.get_merge_requests(1, "test_mr")
        assert result == ["mr1", "mr2"]


@pytest.mark.asyncio
async def test_delete_branch(mock_session):
    with patch(
        "gidgetlab.aiohttp.GitLabAPI.delete", new_callable=AsyncMock
    ) as mock_async_function:
        mock_async_function.return_value = {"status": "deleted"}

        api = GitLabApi(mock_session)
        result = await api.delete_branch(1, "branch_to_delete")
        assert result == {"status": "deleted"}


@pytest.mark.asyncio
async def test_trigger_scanning(mock_session):
    with patch(
        "gidgetlab.aiohttp.GitLabAPI.getitem", new_callable=AsyncMock
    ) as mock_async_function:
        mock_async_function.return_value = {"default_branch": "master"}

        with patch(
            "gidgetlab.aiohttp.GitLabAPI.post", new_callable=AsyncMock
        ) as mock_post_function:
            mock_post_function.return_value = {"pipeline_triggered": True}

            api = GitLabApi(mock_session)
            result = await api.trigger_scanning(1, "container_image")
            assert result == {"pipeline_triggered": True}


@pytest.mark.asyncio
async def test_get_commits(mock_session):
    with patch(
        "gidgetlab.aiohttp.GitLabAPI.getiter", new_callable=MagicMock
    ) as mock_async_function:
        mock_async_function.return_value = ["commit1", "commit2"]

        api = GitLabApi(mock_session)
        results = await api.get_commits(1, "main")

        assert results == ["commit1", "commit2"]
        mock_async_function.assert_called_once_with(
            "/projects/1/repository/commits?ref=main"
        )


@pytest.mark.asyncio
async def test_get_commit(mock_session):
    with patch(
        "gidgetlab.aiohttp.GitLabAPI.getitem", new_callable=AsyncMock
    ) as mock_async_function:
        mock_async_function.return_value = {"commit_data": "details"}

        api = GitLabApi(mock_session)
        result = await api.get_commit(1, "abc123")

        assert result == {"commit_data": "details"}
        mock_async_function.assert_called_once_with(
            "/projects/1/repository/commits/abc123"
        )


@pytest.mark.asyncio
async def test_get_status_checks(mock_session):
    with patch(
        "gidgetlab.aiohttp.GitLabAPI.getitem", new_callable=AsyncMock
    ) as mock_async_function:
        mock_async_function.return_value = [
            {
                "id": 0,
                "name": "test",
                "external_url": "url",
                "status": "status",
            }
        ]

        api = GitLabApi(mock_session)
        result = await api.get_status_checks(1, 0)
        assert result == [
            {
                "id": 0,
                "name": "test",
                "external_url": "url",
                "status": "status",
            }
        ]
        mock_async_function.assert_called_once_with(
            "/projects/1/merge_requests/0/status_checks"
        )


@pytest.mark.asyncio
async def test_set_status_check(mock_session):
    with patch(
        "gidgetlab.aiohttp.GitLabAPI.post", new_callable=AsyncMock
    ) as mock_async_function:
        mock_async_function.return_value = {"id": 0}

        api = GitLabApi(mock_session)
        result = await api.set_status_check(1, 0, "sha", "0", "passed")
        assert result == {"id": 0}


@pytest.mark.asyncio
async def test_modify_job_token_access(mock_session):
    with patch(
        "gidgetlab.aiohttp.GitLabAPI.patch", new_callable=AsyncMock
    ) as mock_async_function:
        mock_async_function.return_value = {}

        api = GitLabApi(mock_session)
        result = await api.modify_job_token_access(1, enabled=True)
        assert result == {}

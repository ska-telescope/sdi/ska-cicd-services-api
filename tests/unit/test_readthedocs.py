# pylint: disable=redefined-outer-name
from unittest.mock import AsyncMock, MagicMock, patch

import pytest

from ska_cicd_services_api.readthedocs_api import HTTPException, ReadTheDocsApi


def create_mock_coroutine(data):
    async def mock_coroutine():
        return data

    return mock_coroutine


@pytest.fixture
def mock_httpx_session():
    return AsyncMock()


@pytest.fixture
def readthedocs_api(mock_httpx_session):
    with patch("httpx.AsyncClient", return_value=mock_httpx_session):
        yield ReadTheDocsApi(session=mock_httpx_session)


@pytest.mark.asyncio
async def test_get_subprojects_list(readthedocs_api, mock_httpx_session):
    mock_response = MagicMock()
    mock_response.status_code = 200
    mock_response.reason_phrase = "Test Exception"
    mock_response.json = MagicMock(
        return_value={"next": None, "results": [{"name": "subproject1"}]}
    )
    mock_httpx_session.get.return_value = mock_response

    subprojects = [
        subproject
        async for subproject in readthedocs_api.get_subprojects_list("project")
    ]
    assert len(subprojects) == 1
    assert subprojects[0]["name"] == "subproject1"


@pytest.mark.asyncio
async def test_get_subproject_detail(readthedocs_api, mock_httpx_session):
    mock_response = MagicMock()
    mock_response.status_code = 200
    mock_response.json = MagicMock(return_value={"name": "subproject1"})
    mock_httpx_session.get.return_value = mock_response

    subproject_detail = await readthedocs_api.get_subproject_detail(
        "project", "subproject"
    )
    assert subproject_detail["name"] == "subproject1"


@pytest.mark.asyncio
async def test_get_subproject_detail_http_exception(
    readthedocs_api, mock_httpx_session
):
    mock_response = MagicMock()
    mock_response.status_code = 404
    mock_response.reason_phrase = "Not Found"
    mock_httpx_session.get.return_value = mock_response

    with pytest.raises(HTTPException) as exc_info:
        await readthedocs_api.get_subproject_detail("project", "subproject")

    assert exc_info.value.status_code == 404
    assert str(exc_info.value) == "Not Found"

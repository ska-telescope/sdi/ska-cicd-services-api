# coding=utf-8
"""Jira API implementation feature tests."""
import logging
import os

import aiohttp
import pytest

from ska_cicd_services_api import readthedocs_api
from ska_cicd_services_api.readthedocs_api import HTTPException
from tests.integration.test_gitlab import get_list_from_async_gen

pytestmark = pytest.mark.asyncio


# VCR Configuration
vcr_log = logging.getLogger("vcr")
vcr_log.setLevel(logging.ERROR)


@pytest.fixture(scope="module")
def vcr_config():
    return {
        "record_mode": "once",
        "filter_headers": ["private-token", "user-agent", "Authorization"],
    }


@pytest.fixture(scope="module")
def vcr_cassette_dir(request):
    return os.path.join(
        "tests/integration/cassettes", request.module.__name__.split(".")[-1]
    )


@pytest.mark.skipif(not os.getenv("RTD_TOKEN"), reason="Need valid token")
def test_check_environment_variable_token():
    assert os.environ["RTD_TOKEN"]


@pytest.mark.vcr
async def test_get_subprojects_list():
    project_slug = "developerskatelescopeorg"

    async with aiohttp.ClientSession() as session:
        api = readthedocs_api.ReadTheDocsApi(session)
        subprojects = api.get_subprojects_list(project_slug)
        subprojects_list = await get_list_from_async_gen(subprojects)
    assert len(subprojects_list) > 0


@pytest.mark.vcr
async def test_get_subprojects_list_fail():
    project_slug = "notexistingprojectslug"
    with pytest.raises(HTTPException) as exc:
        async with aiohttp.ClientSession() as session:
            api = readthedocs_api.ReadTheDocsApi(session)
            subprojects = api.get_subprojects_list(project_slug)
            subprojects_list = await get_list_from_async_gen(subprojects)
            logging.error(
                "Test failed, subproject exists: %s", subprojects_list
            )
    assert 404 == exc.value.status_code


@pytest.mark.vcr
async def test_get_subproject_detail():
    project_slug = "developerskatelescopeorg"
    sub_project_slug = "skampi"
    async with aiohttp.ClientSession() as session:
        api = readthedocs_api.ReadTheDocsApi(session)
        subproject = await api.get_subproject_detail(
            project_slug, sub_project_slug
        )
    assert subproject["_links"] is not None


@pytest.mark.vcr
async def test_get_subproject_detail_fail():
    with pytest.raises(HTTPException) as exc:
        project_slug = "developerskatelescopeorg"
        sub_project_slug = "notexistingproject"
        async with aiohttp.ClientSession() as session:
            api = readthedocs_api.ReadTheDocsApi(session)
            subproject = await api.get_subproject_detail(
                project_slug, sub_project_slug
            )
            logging.error("Test failed, ssubproject exists: %s", subproject)
    assert 404 == exc.value.status_code

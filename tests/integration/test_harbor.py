import logging
import os
from unittest.mock import MagicMock, patch

import pytest
from harborapi.models import Label, Tag

from ska_cicd_services_api.harbor_api import HarborApi

# All test coroutines will be treated as marked.
pytestmark = pytest.mark.asyncio

# VCR Configuration
vcr_log = logging.getLogger("vcr")
vcr_log.setLevel(logging.ERROR)

mock_log_response = MagicMock(return_value=None)


@pytest.fixture(scope="module")
def vcr_config():
    return {
        "record_mode": "once",
        "filter_headers": ["authorization"],
    }


@pytest.fixture(scope="module")
def vcr_cassette_dir(request):
    return os.path.join(
        "tests/integration/cassettes", request.module.__name__.split(".")[-1]
    )


@pytest.mark.vcr
async def test_copy_artifact():
    project_name = "production"
    repository_name = "test"
    reference = "xpto"
    source = f"{project_name}/test-image:{reference}"

    api = HarborApi()
    with patch.object(api.client, "log_response", mock_log_response):
        path = await api.copy_artifact(project_name, repository_name, source)

    assert (
        path
        == "/api/v2.0/projects/production/repositories/test/artifacts/xpto"
    )


@pytest.mark.vcr
async def test_get_repository():
    project_name = "production"
    repository_name = "test"

    api = HarborApi()
    with patch.object(api.client, "log_response", mock_log_response):
        repository = await api.get_repository(project_name, repository_name)

    assert repository.name == "production/test"


@pytest.mark.vcr
async def test_get_repositories():
    project_name = "production"

    api = HarborApi()
    with patch.object(api.client, "log_response", mock_log_response):
        repositories = await api.get_repositories(project_name)

    assert len(repositories) > 0


@pytest.mark.vcr
async def test_get_artifact():
    project_name = "production"
    repository_name = "test"
    reference = "xpto"

    api = HarborApi()
    with patch.object(api.client, "log_response", mock_log_response):
        artifact = await api.get_artifact(
            project_name, repository_name, reference
        )

    assert artifact.id is not None


@pytest.mark.vcr
async def test_get_artifact_tags():
    project_name = "production"
    repository_name = "test"
    reference = "xpto"

    api = HarborApi()
    with patch.object(api.client, "log_response", mock_log_response):
        tags = await api.get_artifact_tags(
            project_name, repository_name, reference
        )

    assert tags[0].name == "xpto"


@pytest.mark.vcr
async def test_create_artifact_tag():
    project_name = "production"
    repository_name = "test"
    reference = "xpto"
    test_tag = Tag(name="test_tag")

    api = HarborApi()
    with patch.object(api.client, "log_response", mock_log_response):
        await api.create_artifact_tag(
            project_name, repository_name, reference, test_tag
        )


@pytest.mark.vcr
async def test_delete_artifact_tag():
    project_name = "production"
    repository_name = "test"
    reference = "xpto"
    tag = "test_tag"

    api = HarborApi()
    with patch.object(api.client, "log_response", mock_log_response):
        await api.delete_artifact_tag(
            project_name, repository_name, reference, tag
        )


@pytest.mark.vcr
async def test_get_labels():
    api = HarborApi()
    with patch.object(api.client, "log_response", mock_log_response):
        labels = await api.get_labels()
    assert labels[0].name == "test_label"


@pytest.mark.vcr
async def test_add_artifact_label():
    project_name = "production"
    repository_name = "test"
    reference = "xpto"
    label = Label(id=1)

    api = HarborApi()
    with patch.object(api.client, "log_response", mock_log_response):
        await api.add_artifact_label(
            project_name, repository_name, reference, label
        )


@pytest.mark.vcr
async def test_delete_artifact_label():
    project_name = "production"
    repository_name = "test"
    reference = "xpto"
    label_id = 1

    api = HarborApi()
    with patch.object(api.client, "log_response", mock_log_response):
        await api.delete_artifact_label(
            project_name, repository_name, reference, label_id
        )


@pytest.mark.vcr
async def test_delete_artifact():
    project_name = "production"
    repository_name = "test"
    reference = "xpto"

    api = HarborApi()
    with patch.object(api.client, "log_response", mock_log_response):
        await api.delete_artifact(project_name, repository_name, reference)

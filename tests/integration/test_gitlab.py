import logging
import os

import aiohttp
import pytest

from ska_cicd_services_api import gitlab_api

# All test coroutines will be treated as marked.
pytestmark = pytest.mark.asyncio

# VCR Configuration
vcr_log = logging.getLogger("vcr")
vcr_log.setLevel(logging.ERROR)


@pytest.fixture(scope="module")
def vcr_config():
    return {
        "record_mode": "once",
        "filter_headers": ["private-token", "user-agent"],
    }


@pytest.fixture(scope="module")
def vcr_cassette_dir(request):
    return os.path.join(
        "tests/integration/cassettes", request.module.__name__.split(".")[-1]
    )


async def get_list_from_async_gen(async_generator):
    return [i async for i in async_generator]


@pytest.mark.skipif(
    not os.getenv("GITLAB_API_REQUESTER"), reason="Need valid requester"
)
def test_check_environment_variable_requester():
    assert os.environ["GITLAB_API_REQUESTER"]


@pytest.mark.skipif(
    not os.getenv("GITLAB_API_PRIVATE_TOKEN"), reason="Need valid token"
)
def test_check_environment_variable_token():
    assert os.environ["GITLAB_API_PRIVATE_TOKEN"]


@pytest.mark.vcr
async def test_get_commit_diffs():
    proj_id = 22233142
    commit_id = "297796bb"

    async with aiohttp.ClientSession() as session:
        api = gitlab_api.GitLabApi(session)
        commit_diffs = await api.get_commit_diffs(proj_id, commit_id)
        commit_diff_list = await get_list_from_async_gen(commit_diffs)

    expected_diff = (
        "@@ -8,7 +8,7 @@ with open('README.md') as readme_file"
        ":\n \n setup(\n     name='ska-cicd-services-api',\n-    version="
        "'0.2.0',\n+    version='0.3.0',\n     description=\"\",\n     "
        "long_description=readme + '\\n\\n',\n     author=\"Matteo Di "
        'Carlo, Adriaan de Beer, Ugur Yilmaz",\n'
    )
    assert commit_diff_list[0]["diff"] == expected_diff


@pytest.mark.vcr
async def test_add_comment():
    proj_id = 22233142
    mr_id = 8
    comment = "test comment"

    async with aiohttp.ClientSession() as session:
        api = gitlab_api.GitLabApi(session)
        result = await api.add_comment(proj_id, mr_id, comment)

    assert comment == result["body"]


@pytest.mark.vcr
async def test_remove_comment():
    proj_id = 22233142
    mr_id = 8  # iid
    comment_id = 521359266

    async with aiohttp.ClientSession() as session:
        api = gitlab_api.GitLabApi(session)
        await api.remove_comment(proj_id, mr_id, comment_id)

    assert True  # if no exception is thrown then test passed!


@pytest.mark.vcr
async def test_get_merge_request_info():
    proj_id = 22233142
    mr_id = 8
    title = "ST-588: improve feedback mechanism"

    async with aiohttp.ClientSession() as session:
        api = gitlab_api.GitLabApi(session)
        mr_info = await api.get_merge_request_info(proj_id, mr_id)

    assert title in mr_info["title"]


@pytest.mark.vcr
async def test_get_merge_request_approvals_info():
    proj_id = 22233142
    mr_id = 8

    async with aiohttp.ClientSession() as session:
        api = gitlab_api.GitLabApi(session)
        mr_approvals = await api.get_merge_request_approvals_info(
            proj_id, mr_id
        )

    assert mr_approvals["approvals_required"] > 0


@pytest.mark.vcr
async def test_get_project_merge_request_approvals_info():
    proj_id = 22233142

    async with aiohttp.ClientSession() as session:
        api = gitlab_api.GitLabApi(session)
        proj_mr_approvals = await api.get_project_merge_request_approvals_info(
            proj_id
        )

    assert proj_mr_approvals["reset_approvals_on_push"]


@pytest.mark.vcr
async def test_get_merge_request_commits():
    proj_id = 22233142
    mr_id = 8
    number_of_commits = 11

    async with aiohttp.ClientSession() as session:
        api = gitlab_api.GitLabApi(session)
        mr_commits = await api.get_merge_request_commits(proj_id, mr_id)
        mr_commits_list = await get_list_from_async_gen(mr_commits)

    assert len(mr_commits_list) == number_of_commits


@pytest.mark.vcr
async def test_get_project_settings_info():
    proj_id = 22233142
    name = "SKA CICD Services API"

    async with aiohttp.ClientSession() as session:
        api = gitlab_api.GitLabApi(session)
        proj_info = await api.get_project_settings_info(proj_id)

    assert proj_info["name"] == name


@pytest.mark.vcr
async def test_get_project_license():
    proj_id = 23664719
    license_key = "bsd-3-clause"

    async with aiohttp.ClientSession() as session:
        api = gitlab_api.GitLabApi(session)
        proj_info = await api.get_project_settings_info(
            proj_id, with_license=True
        )

    assert license_key in proj_info["license"]["key"]


@pytest.mark.vcr
async def test_get_project_members():
    proj_id = 22233142

    async with aiohttp.ClientSession() as session:
        api = gitlab_api.GitLabApi(session)
        proj_members = await api.get_project_members(proj_id)
        proj_members_list = await get_list_from_async_gen(proj_members)

    assert len(proj_members_list) > 0


@pytest.mark.vcr
async def test_get_merge_request_comments():
    proj_id = 22233142
    mr_id = 8
    number_of_comments = 22

    async with aiohttp.ClientSession() as session:
        api = gitlab_api.GitLabApi(session)
        mr_comments = await api.get_merge_request_comments(proj_id, mr_id)
        mr_comments_list = await get_list_from_async_gen(mr_comments)

    assert len(mr_comments_list) == number_of_comments


@pytest.mark.vcr
async def test_get_comment_info():
    proj_id = 22233142
    mr_id = 8
    comment_id = 488712981
    comment_text = "'test comment for unit testing'"

    async with aiohttp.ClientSession() as session:
        api = gitlab_api.GitLabApi(session)
        mr_comment = await api.get_comment_info(proj_id, mr_id, comment_id)

    assert mr_comment["body"] == comment_text


@pytest.mark.vcr
async def test_modify_comment():
    proj_id = 22233142
    mr_id = 8
    comment_id = 503277186
    comment_text = "test modify comment for unit testing"

    async with aiohttp.ClientSession() as session:
        api = gitlab_api.GitLabApi(session)
        mr_comment = await api.modify_comment(
            proj_id, mr_id, comment_id, comment_text
        )

    assert mr_comment["body"] == comment_text


@pytest.mark.vcr
async def test_get_search_results():
    proj_id = 22233142
    scope = "blobs"
    search = "tests"
    number_of_results = 118

    async with aiohttp.ClientSession() as session:
        api = gitlab_api.GitLabApi(session)
        search_results = await api.get_search_results(proj_id, scope, search)
        search_results_list = await get_list_from_async_gen(search_results)

    assert len(search_results_list) == number_of_results


@pytest.mark.vcr
async def test_get_file_from_repository():
    proj_id = 22233142
    ref = "master"
    file_path = ".gitlab-ci.yml"
    filename = file_path

    file_path = file_path.replace("/", "%2F")

    async with aiohttp.ClientSession() as session:
        api = gitlab_api.GitLabApi(session)
        file_result = await api.get_file_from_repository(
            proj_id, file_path, ref
        )

    assert file_result["file_name"] == filename


@pytest.mark.vcr
async def test_get_repo_tree():
    proj_id = 18513030
    path_to_check = "docs"
    result = False

    async with aiohttp.ClientSession() as session:
        api = gitlab_api.GitLabApi(session)
        repo_tree = await api.get_repo_tree(proj_id)
        async for tree in repo_tree:
            if path_to_check in tree["path"]:
                result = True

    assert result


@pytest.mark.vcr
async def test_get_repo_tree_recursive():
    proj_id = 22233142  # services project id
    path_to_check = "docs/src"
    result = False

    async with aiohttp.ClientSession() as session:
        api = gitlab_api.GitLabApi(session)
        repo_tree = await api.get_repo_tree(proj_id, recursive=True)
        async for tree in repo_tree:
            if path_to_check in tree["path"]:
                result = True

    assert result


@pytest.mark.vcr
async def test_get_mr_diff_versions():
    proj_id = 22233142
    mr_id = 8

    async with aiohttp.ClientSession() as session:
        api = gitlab_api.GitLabApi(session)
        mr_diff_versions = await api.get_mr_diff_versions(proj_id, mr_id)
        mr_diff_versions_list = await get_list_from_async_gen(mr_diff_versions)

    assert len(mr_diff_versions_list) > 0


@pytest.mark.vcr
async def test_get_single_mr_diff_version():
    proj_id = 22233142
    mr_id = 8
    version_id = 138144569

    async with aiohttp.ClientSession() as session:
        api = gitlab_api.GitLabApi(session)
        mr_diff_version = await api.get_single_single_mr_diff_version(
            proj_id, mr_id, version_id
        )

    assert mr_diff_version["state"] == "collected"


@pytest.mark.vcr
async def test_get_project_hooks():
    proj_id = 11448712
    hook_to_check = "readthedocs.org"
    result = False

    async with aiohttp.ClientSession() as session:
        api = gitlab_api.GitLabApi(session)
        proj_hooks = await api.get_project_hooks(proj_id)
        async for hook in proj_hooks:
            if hook_to_check in hook["url"]:
                result = True

    assert result


@pytest.mark.vcr
async def test_create_mr():
    proj_id = 22408582
    source_branch = "test"
    target_branch = "master"
    title = "Test Merge Request"
    assignee_ids = [7312977]
    reviewer_ids = [7312977]
    description = "This is a test merge request"
    labels = ""
    remove_source_branch = True
    squash = False

    async with aiohttp.ClientSession() as session:
        api = gitlab_api.GitLabApi(session)
        mr = await api.create_mr(
            proj_id=proj_id,
            source_branch=source_branch,
            target_branch=target_branch,
            title=title,
            assignee_ids=assignee_ids,
            reviewer_ids=reviewer_ids,
            description=description,
            labels=labels,
            remove_source_branch=remove_source_branch,
            squash=squash,
        )

        assert mr["project_id"] == 22408582
        assert mr["state"] == "opened"
        assert mr["title"] == "Test Merge Request"
        assert mr["description"] == "This is a test merge request"
        assert mr["source_branch"] == "test"
        assert mr["target_branch"] == "master"
        assert mr["author"]["name"] == "Diogo Regateiro"
        assert mr["assignees"][0]["name"] == "Diogo Regateiro"
        assert mr["reviewers"][0]["name"] == "Diogo Regateiro"
        assert mr["labels"] == []
        assert mr["force_remove_source_branch"] is True
        assert mr["squash"] is False

    assert mr


@pytest.mark.vcr
async def test_modify_mr():
    proj_id = 22408582
    mr_id = 16
    state = "close"
    target_branch = "master"
    title = "Test Merge Request"
    assignee_ids = [7312977]
    reviewer_ids = [7312977]
    description = "This is a test merge request"
    labels = ""
    remove_source_branch = True
    squash = False

    async with aiohttp.ClientSession() as session:
        api = gitlab_api.GitLabApi(session)
        mr = await api.modify_merge_request(
            proj_id=proj_id,
            mr_id=mr_id,
            title=title,
            state_event=state,
            assignee_ids=assignee_ids,
            reviewer_ids=reviewer_ids,
            target_branch=target_branch,
            description=description,
            labels=labels,
            remove_source_branch=remove_source_branch,
            squash=squash,
        )

        assert mr["project_id"] == proj_id
        assert mr["state"] == "closed"
        assert mr["title"] == title
        assert mr["description"] == description
        assert mr["target_branch"] == target_branch
        assert mr["assignees"][0]["name"] == "Diogo Regateiro"
        assert mr["reviewers"][0]["name"] == "Diogo Regateiro"
        assert mr["labels"] == []
        assert mr["force_remove_source_branch"] is True
        assert mr["squash"] is False

    assert mr


@pytest.mark.vcr
async def test_modify_project_level_mr_approval_settings():
    proj_id = 22408582
    disable_overriding_approvers_per_merge_request = False
    reset_approvals_on_push = True
    merge_requests_author_approval = False
    approvals_required = 1

    async with aiohttp.ClientSession() as session:
        api = gitlab_api.GitLabApi(session)
        settings = await api.modify_project_level_mr_approval_settings(
            proj_id=proj_id,
            disable_overriding_approvers_per_merge_request=disable_overriding_approvers_per_merge_request,  # NOQA: E501
            reset_approvals_on_push=reset_approvals_on_push,
            merge_requests_author_approval=merge_requests_author_approval,
            approvals_before_merge=approvals_required,
        )

        assert (
            settings["disable_overriding_approvers_per_merge_request"] is False
        )
        assert settings["reset_approvals_on_push"] is True
        assert settings["merge_requests_author_approval"] is False
        assert settings["approvals_before_merge"] == approvals_required

    assert settings


@pytest.mark.vcr
async def test_modify_mr_level_mr_approval_settings():
    proj_id = 22408582
    mr_id = 16
    approvals_required = 1

    async with aiohttp.ClientSession() as session:
        api = gitlab_api.GitLabApi(session)
        settings = await api.modify_mr_level_mr_approval_settings(
            proj_id=proj_id, mr_id=mr_id, approvals_required=approvals_required
        )

        assert settings["project_id"] == proj_id
        assert settings["iid"] == mr_id
        assert settings["approvals_required"] == approvals_required

    assert settings


@pytest.mark.vcr
async def test_modify_project():
    proj_id = 22408582
    merge_method = "merge"
    resolve_outdated_diff_discussions = True
    printing_merge_request_link_enabled = True
    remove_source_branch_after_merge = True
    only_allow_merge_if_pipeline_succeeds = True

    async with aiohttp.ClientSession() as session:
        api = gitlab_api.GitLabApi(session)
        settings = await api.modify_project(
            proj_id=proj_id,
            merge_method=merge_method,
            resolve_outdated_diff_discussions=resolve_outdated_diff_discussions,  # NOQA: E501
            printing_merge_request_link_enabled=printing_merge_request_link_enabled,  # NOQA: E501
            remove_source_branch_after_merge=remove_source_branch_after_merge,
            only_allow_merge_if_pipeline_succeeds=only_allow_merge_if_pipeline_succeeds,  # NOQA: E501
        )

        assert settings["id"] == proj_id
        assert settings["merge_method"] == merge_method
        assert settings["resolve_outdated_diff_discussions"] is True
        assert settings["printing_merge_request_link_enabled"] is True
        assert settings["remove_source_branch_after_merge"] is True
        assert settings["only_allow_merge_if_pipeline_succeeds"] is True

    assert settings


@pytest.mark.vcr
async def test_create_branch():
    proj_id = 22233142
    branch_name = "new-test"
    ref = "master"

    async with aiohttp.ClientSession() as session:
        api = gitlab_api.GitLabApi(session)
        branch = await api.create_branch(
            proj_id=proj_id, branch=branch_name, ref=ref
        )

        assert branch["name"] == branch_name

    assert branch


@pytest.mark.vcr
async def test_delete_branch():
    proj_id = 22233142
    branch_name = "new-test"

    async with aiohttp.ClientSession() as session:
        api = gitlab_api.GitLabApi(session)
        await api.delete_branch(proj_id=proj_id, branch=branch_name)


@pytest.mark.vcr
async def test_get_current_user():
    async with aiohttp.ClientSession() as session:
        api = gitlab_api.GitLabApi(session)
        # ST-1030: Cannot test with access_token as they expire
        result = await api.get_current_user()

    assert result
    assert result["username"] == "marvin-42"
    assert "error" not in result


@pytest.mark.vcr
async def test_get_merge_requests():
    # use ska-cicd-services-api repo for the test
    proj_id = 22233142
    title = "DRAFT: ST-875"

    async with aiohttp.ClientSession() as session:
        api = gitlab_api.GitLabApi(session)
        mr = await api.get_merge_requests(proj_id=proj_id, mr_title=title)
        mr_list = await get_list_from_async_gen(mr)
    assert mr_list[0]


@pytest.mark.vcr
async def test_get_merge_requests_not():
    # use ska-cicd-services-api repo for the test
    proj_id = 22233142
    title = "DRAFT: ST-876"

    async with aiohttp.ClientSession() as session:
        api = gitlab_api.GitLabApi(session)
        mr = await api.get_merge_requests(proj_id=proj_id, mr_title=title)
        mr_list = await get_list_from_async_gen(mr)
    assert not mr_list


@pytest.mark.vcr
async def test_trigger_scanning():
    proj_id = 9027158
    cont_image = "artefact.skao.int/ska-tango-images-tango-itango:9.3.6"
    async with aiohttp.ClientSession() as session:
        api = gitlab_api.GitLabApi(session)
        await api.trigger_scanning(proj_id=proj_id, cont_image=cont_image)
    assert True


@pytest.mark.vcr
async def test_approve_mr():
    # use ska-cicd-automation repo for the test
    proj_id = 23664719
    mr_id = 17
    async with aiohttp.ClientSession() as session:
        api = gitlab_api.GitLabApi(session)
        await api.add_approval_merge_request(proj_id, mr_id)
    assert True


@pytest.mark.vcr
async def test_unapprove_mr():
    # use ska-cicd-automation repo for the test
    proj_id = 23664719
    mr_id = 17
    async with aiohttp.ClientSession() as session:
        api = gitlab_api.GitLabApi(session)
        await api.remove_approval_merge_request(proj_id, mr_id)
    assert True


@pytest.mark.vcr
async def test_get_job_artifacts():
    # use ska-cicd-automation repo for the test
    proj_id = 23664719
    # job https://gitlab.com/ska-telescope/sdi/ska-cicd-automation/-/jobs/1943798937 # NOQA: E501
    job_id = 1943798937

    async with aiohttp.ClientSession() as session:
        api = gitlab_api.GitLabApi(session)
        artefacts = await api.get_job_artifacts(proj_id, job_id)

    assert isinstance(artefacts, bytes)

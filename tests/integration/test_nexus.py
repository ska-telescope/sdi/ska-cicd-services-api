import logging
import os

import aiohttp
import pytest

from ska_cicd_services_api import nexus_api
from ska_cicd_services_api.nexus_api import HTTPException

# All test coroutines will be treated as marked.
pytestmark = pytest.mark.asyncio

# VCR Configuration
vcr_log = logging.getLogger("vcr")
vcr_log.setLevel(logging.ERROR)


@pytest.fixture(scope="module")
def vcr_config():
    return {
        "record_mode": "once",
        "filter_headers": ["private-token", "user-agent"],
    }


@pytest.fixture(scope="module")
def vcr_cassette_dir(request):
    return os.path.join(
        "tests/integration/cassettes", request.module.__name__.split(".")[-1]
    )


async def get_list_from_async_gen(async_generator):
    return [i async for i in async_generator]


@pytest.mark.vcr
async def test_get_components_list():
    repository = "docker-internal"

    async with aiohttp.ClientSession() as session:
        api = nexus_api.NexusApi(session)
        artefacts = await api.get_components_list(repository)
        artefacts_list = await get_list_from_async_gen(artefacts)
    assert len(artefacts_list) > 0


@pytest.mark.vcr
async def test_get_components_list_fail():
    repository = "notexistingrepo"
    with pytest.raises(HTTPException) as exc:
        async with aiohttp.ClientSession() as session:
            api = nexus_api.NexusApi(session)
            artefacts = await api.get_components_list(repository)
            artefacts_list = await get_list_from_async_gen(artefacts)
            logging.error("Test failed, repo does exist: %s", artefacts_list)
    assert 404 == exc.value.status_code


@pytest.mark.vcr
async def test_get_component_info():
    componentId = "cHlwaS1pbnRlcm5hbDo4NTEzNTY1MmE5NzhiZTlhNzU4YmM4MzE1NzBiYzViNg"  # NOQA: E501
    async with aiohttp.ClientSession() as session:
        api = nexus_api.NexusApi(session)
        component = await api.get_component_info(componentId)
    assert componentId == component["id"]


@pytest.mark.vcr
async def test_get_component_info_fail():
    componentId = (
        "cHlwaS1pbnRlcm5hbDo4NTEzNTY1MmE5NzhiZTlhNzU41mM4MzE1NzBiYzViN"
    )
    with pytest.raises(HTTPException) as exc:
        async with aiohttp.ClientSession() as session:
            api = nexus_api.NexusApi(session)
            component = await api.get_component_info(componentId)
            logging.error("Test failed, component does exist: %s", component)
    assert 404 == exc.value.status_code


@pytest.mark.vcr
async def test_remove_component():
    componentId = "ZG9ja2VyLWludGVybmFsOjg4NDkxY2QxZDE4NWRkMTMwYjUyNzQ1M2FhYTRkMmRh"  # NOQA: E501
    async with aiohttp.ClientSession() as session:
        api = nexus_api.NexusApi(session, url="http://localhost:8081")
        component = await api.remove_component(componentId)
    assert {"result": "successfull"} == component


@pytest.mark.vcr
async def test_remove_component_fail():
    # Note: this id is just one letter change at the end of a valid id
    # any other change results in a 422: Unprocessed Entity
    componentId = (
        "ZG9ja2VyLWludGVybmFsOjJhNTkwNDNlZDJlYTU1NmUyNGEyNjhiZjkxZDdlNDk1"
    )
    with pytest.raises(HTTPException) as exc:
        async with aiohttp.ClientSession() as session:
            api = nexus_api.NexusApi(session, url="http://localhost:8081")
            component = await api.get_component_info(componentId)
            logging.error("Test failed, component does exist: %s", component)
    assert 404 == exc.value.status_code


# TODO: enable this in future!
# vcrpy doesn't support aiohttp post requests
# See https://github.com/kevin1024/vcrpy/pull/572
# or https://github.com/kevin1024/vcrpy/pull/582
#
# @pytest.mark.vcr
# async def test_upload_component():
#     repository = "raw-internal"
#     # breakpoint()
#     file = open("tests/unit/files/test_upload_component.txt", 'rb')
#     async with aiohttp.ClientSession() as session:
#         api = nexus.NexusApi(session)
#         component = await api.upload_component(
#             repository_type="raw",
#             repository=repository,
#             raw_asset1=file,
#             raw_asset1_filename="test_upload_component.txt",
#             raw_directory="tmp/apitest"
#         )
#     assert {"result": "successfull"} == component

"""Jira API implementation feature tests."""

import logging
import os

import pytest
from jira.exceptions import JIRAError

from ska_cicd_services_api import jira_api

pytestmark = pytest.mark.asyncio


# VCR Configuration
vcr_log = logging.getLogger("vcr")
vcr_log.setLevel(logging.ERROR)


@pytest.fixture(scope="module")
def vcr_config():
    return {
        "record_mode": "once",
        "filter_headers": ["private-token", "user-agent", "Authorization"],
    }


@pytest.fixture(scope="module")
def vcr_cassette_dir(request):
    return os.path.join(
        "tests/integration/cassettes", request.module.__name__.split(".")[-1]
    )


@pytest.mark.vcr
@pytest.mark.skipif(
    not os.getenv("JIRA_PASSWORD"), reason="Need valid username and password"
)
async def test_auth():
    jira_obj = jira_api.JiraApi()
    logging.info("Connecting to %s", os.environ["JIRA_URL"])
    await jira_obj.authenticate()
    logging.info(jira_obj.connection_details)
    assert (
        jira_obj.connection_details
        == b"U2VydmVyOiBodHRwczovL2ppcmEuc2thdGVsZXNjb3BlLm9yZwpVc2VyOiBEZXZQb3J0YWwtU2VydmljZXMKUGFzczogRXYyM20tNzE2aGx1UFBA"  # NOQA: E501
    ), "Connection details incorrect"


@pytest.mark.vcr
async def test_get_projects():
    jira_obj = jira_api.JiraApi()
    await jira_obj.authenticate()
    projects = await jira_obj.get_projects()
    project_list = [project["key"] for project in projects]
    assert (
        "No_project_should_be_called_this" not in project_list
    ), "Really, this test should not pass"
    assert "CHR" in project_list, f"Project CHR not found in {project_list}"


@pytest.mark.vcr
async def test_get_issue():
    jira_obj = jira_api.JiraApi()
    await jira_obj.authenticate()
    issue = await jira_obj.get_issue()
    logging.info("Issue summary: %s", issue.fields.summary)
    assert issue.fields.summary == "JIRA GitLab Test Issue"


@pytest.mark.vcr
async def test_fail_to_get_an_issue():
    with pytest.raises(JIRAError) as err:
        jira_obj = jira_api.JiraApi()
        await jira_obj.authenticate()
        issue_result = await jira_obj.get_issue("CHR-0")
        logging.info("Test failed, issue exists: %s", issue_result)
    assert "Issue Does Not Exist" in str(err.value)


@pytest.mark.vcr
async def test_unable_to_create_issues_with_wrong_issue_type():
    jira_obj = jira_api.JiraApi()
    await jira_obj.authenticate()
    issue_dict = {
        "project": {"key": "CHR"},
        "summary": "New issue from jira-python",
        "description": "Look into this one",
        "issuetype": {"name": "Bug"},
    }
    issue_list = [issue_dict]
    created_issues = await jira_obj.create_issues(issue_list)
    logging.info(
        "Issue creation result (should have status Error): %s", created_issues
    )
    assert created_issues[0]["status"] == "Error"
    assert created_issues[0]["error"]["issuetype"] is not None


@pytest.mark.vcr
async def test_comment_on_existing_issue_with_issue_key():
    jira_obj = jira_api.JiraApi()
    await jira_obj.authenticate()
    comment = "This is a test comment"
    comment_obj = await jira_obj.comment_on_issue(
        issue="ST-443", comment=comment
    )
    assert comment_obj is not None

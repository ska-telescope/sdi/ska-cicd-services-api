import json
import logging
import os
import traceback
from collections import defaultdict
from typing import Any

from cachetools import TTLCache
from google.oauth2 import service_account
from google.oauth2.credentials import Credentials
from googleapiclient import discovery
from pydantic import BaseModel

logger = logging.getLogger("PeopleDatabaseApi")


class PeopleDatabaseField(BaseModel):
    """
    PeopleDatabaseField describes how to get a field from the People's
    database and if it is unique. If it is unique, we will build a map
    from it to expedite searching

    column: Column id in the spreadsheet
    unique: True if field is unique in the database, False otherwise
    """

    column: Any
    unique: bool = False

    def model_post_init(self, _):
        self.column = int(self.column)


class PeopleDatabaseColumnMapping(BaseModel):
    """
    PeopleDatabaseColumnMapping describes the base model for the People's
    DB column mapping. It can be extended by inheriting the class and adding
    other fields
    """

    name: PeopleDatabaseField = PeopleDatabaseField(
        column=os.getenv("PEOPLE_DB_NAME_COLUMN", "1")
    )
    email: PeopleDatabaseField = PeopleDatabaseField(
        column=os.getenv("PEOPLE_DB_EMAIL_COLUMN", "2"),
        unique=True,
    )
    team: PeopleDatabaseField = PeopleDatabaseField(
        column=os.getenv("PEOPLE_DB_TEAM_COLUMN", "3")
    )
    slack_id: PeopleDatabaseField = PeopleDatabaseField(
        column=os.getenv("PEOPLE_DB_SLACK_ID_COLUMN", "4"),
        unique=True,
    )
    gitlab_handle: PeopleDatabaseField = PeopleDatabaseField(
        column=os.getenv("PEOPLE_DB_GITLAB_HANDLE_COLUMN", "5"),
        unique=True,
    )


class PeopleDatabaseUser(BaseModel):
    """
    PeopleDatabaseUser has the information we gather from the People's
    database that is useful for communications and labelling operations.
    It can be extended by inheriting the class and adding other fields.
    MUST MATCH PeopleDatabaseColumnMapping
    """

    name: str
    email: str
    team: str
    slack_id: str
    gitlab_handle: str


class PeopleDatabaseApi:
    """
    A class to interact with the Google Sheets-based People Database.

    spreadsheet_id: Id of the spreadsheet
    spreadsheet_range: Spreadsheet range in A1 notation
    credentials: Google Service Account credentials
    cache: Cache object
    column_mapping: Mapping of columns names to ids
    """

    spreadsheet_id: int
    spreadsheet_range: str
    credentials: Credentials
    cache: TTLCache
    column_mapping: PeopleDatabaseColumnMapping
    user_class: type
    service: Any
    cached_mappings: defaultdict

    def __init__(
        self,
        service_account_data: dict | str = os.getenv("PEOPLE_DB_CREDENTIALS"),
        spreadsheet_id: str = os.getenv("PEOPLE_DB_SHEET_ID"),
        spreadsheet_range: str = os.getenv(
            "PEOPLE_DB_SHEET_RANGE", "A2:Z1000"
        ),
        cache_ttl: float = float(os.getenv("PEOPLE_DB_CACHE_TTL", "1800")),
        column_mapping=PeopleDatabaseColumnMapping(),
        user_class=PeopleDatabaseUser,
    ):
        """
        Initialize the PeopleDatabaseApi instance.

        :param service_account_data: Service account credentials for
        GoogleSheets API
        :param spreadsheet_id: Id of the spreadsheet
        :param spreadsheet_range: Range in A1 notation to cache
        :param cache_ttl: TTL for the cache in seconds
        :param column_mapping: Columns mapping to work with. It is a
        field to allow the API to be extended from the client side
        :param cached_mappings: Cached mappings by column mapping to
        make searching faster
        """
        service_account_config = service_account_data
        if isinstance(service_account_data, str):
            service_account_config = json.loads(service_account_data)

        self.credentials = (
            service_account.Credentials.from_service_account_info(
                service_account_config
            )
        )
        self.service = discovery.build(
            "sheets", "v4", credentials=self.credentials
        )
        self.cache = TTLCache(maxsize=1, ttl=cache_ttl)
        self.spreadsheet_id = spreadsheet_id
        self.spreadsheet_range = spreadsheet_range
        self.column_mapping = column_mapping
        self.user_class = user_class
        self.cached_mappings = defaultdict(
            lambda: defaultdict(self.user_class)
        )

    def _cache_available(self) -> bool:
        return (
            "sheet_content" in self.cache
            and len(self.cache["sheet_content"]) > 0
        )

    def _extract_user(self, data: list[str]) -> PeopleDatabaseUser | None:
        if data is None or len(data) == 0:
            return None

        user_data = {}
        self.user_class: BaseModel
        for field in self.user_class.model_fields:
            field_column = max(
                getattr(self.column_mapping, field).column - 1, 0
            )
            if field_column >= len(data):
                user_data[field] = ""
            else:
                user_data[field] = data[field_column]

        return self.user_class(**user_data)

    async def _get_sheet(
        self, by_field: str = None
    ) -> dict[str, PeopleDatabaseUser] | list:
        """
        Fetch the sheet content from the Google Sheets API.

        Returns:
            list: The content of the sheet.
        """
        if self._cache_available():
            if by_field and by_field in self.cached_mappings:
                return self.cached_mappings[by_field]

            return self.cache["sheet_content"]

        try:
            response = (
                self.service.spreadsheets()  # pylint: disable=no-member
                .values()
                .get(
                    spreadsheetId=self.spreadsheet_id,
                    range=self.spreadsheet_range,
                )
                .execute()
            )
            sheet_content = response.get("values", [])
            self.cache["sheet_content"] = sheet_content
            unique_fields = [
                field
                for field in self.column_mapping.model_fields
                if getattr(self.column_mapping, field).unique
            ]

            # Reset the cache
            self.cached_mappings = defaultdict(
                lambda: defaultdict(self.user_class)
            )

            # Build a chache per unique field to speedup lookups
            for user_data in sheet_content:
                user: PeopleDatabaseUser = self._extract_user(user_data)
                if user:
                    for field in unique_fields:
                        if getattr(user, field) not in [None, ""]:
                            self.cached_mappings[field][
                                getattr(user, field)
                            ] = user

            return await self._get_sheet(by_field)
        except Exception as exc:  # pylint: disable=broad-exception-caught
            logger.error(
                "Error fetching spreadsheet '%s' data: %s",
                self.spreadsheet_id,
                exc,
            )
            traceback.print_exception(exc)
            return []

    async def get_user(
        self, field: str, value: str
    ) -> PeopleDatabaseUser | None:
        """
        Get a user by looking into a specific unique field

        :param field: Field to search
        :param value: Value to match
        :return: The matched user or None if not found
        """
        sheet_data = await self._get_sheet(field)
        return sheet_data.get(value, None)

    async def get_user_by_email(self, email: str) -> PeopleDatabaseUser | None:
        """
        Get a user by email

        :param email: Email of the user
        :return: The matched user or None if not found
        """
        return await self.get_user("email", email)

    async def get_user_by_slack_id(
        self, slack_id: str
    ) -> PeopleDatabaseUser | None:
        """
        Get a user by gitlab_handle

        :param gitlab_handle: Gitlab handle of the user
        :return: The matched user or None if not found
        """
        return await self.get_user("slack_id", slack_id)

    async def get_user_by_gitlab_handle(
        self, gitlab_handle: str
    ) -> PeopleDatabaseUser | None:
        """
        Get a user by gitlab_handle

        :param gitlab_handle: Gitlab handle of the user
        :return: The matched user or None if not found
        """
        return await self.get_user("gitlab_handle", gitlab_handle)

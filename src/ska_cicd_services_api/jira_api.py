import asyncio
import base64
import os

from jira import JIRA


def async_wrapper(cls):
    class AsyncWrapper:
        def __init__(self, *args, **kwargs):
            self.sync_instance = cls(*args, **kwargs)

        def __getattr__(self, name):
            method = getattr(self.sync_instance, name)
            if asyncio.iscoroutinefunction(method):
                return method

            if callable(method):

                async def async_method(*args, **kwargs):
                    return await asyncio.to_thread(method, *args, **kwargs)

                return async_method

            return method

    return AsyncWrapper


class JiraApi:
    """JiraApi class implementing the async wrapper of python-jira.
    Interacts with the JIRA api and authenticates using environment variables.

    For more information, see https://pypi.org/project/aiojira/
    """

    def __init__(self):
        self.jira = None
        self.connection_details = None

    async def authenticate(self, jira_url=None, user=None, password=None):
        """Authenticate against the JIRA Api using basic auth.
        Call this method before continuing to interact with the API.

        :param jira_url: URL of the Jira instance, defaults to None
        :type jira_url: str, optional

        :param user: Username of user that can log into JIRA, defaults to None
        :type user: str, optional

        :param password: Password for user to log into JIRA, defaults to None
        :type password: str, optional
        """

        if not jira_url:
            jira_url = os.environ["JIRA_URL"]
        if not user:
            user = os.environ["JIRA_USERNAME"]
        if not password:
            password = os.environ["JIRA_PASSWORD"]

        msg = f"Server: {jira_url}\nUser: {user}\nPass: {password}"
        self.connection_details = base64.b64encode(msg.encode("ascii"))

        AsyncJIRA = async_wrapper(JIRA)
        self.jira = AsyncJIRA(server=jira_url, basic_auth=(user, password))

    async def get_projects(self, projectType: str = None):
        """Get the list of allowed projects by type

        :param projectType: Type of projects to filter on, defaults to None
        :type projectType: str, optional
        :return: list of projects not ignored -
            setting projectType to empty string will return all projects
        :rtype: list
        """
        projects = await self.jira.projects()
        projects = [
            p
            for p in projects
            if not projectType or p.projectTypeKey == projectType
        ]
        return [
            {
                "key": p.key,
                "id": int(p.id),
                "name": p.name,
                "type": p.projectTypeKey,
            }
            for p in projects
            if p.key != os.getenv("IGNORE_PROJECTS")
        ]

    async def get_issue(self, issue_key: str = "ST-443"):
        """Get issue by key

        :param issue_key: Jira issue key, defaults to 'ST-443'
        :type issue_key: str, optional
        :return: JIRA issue object
        :rtype: object
        """
        issue = await self.jira.issue(issue_key)
        return issue

    async def create_issues(self, issue_list: dict):
        """Create one or multiple issues.

        :param issue_list: list of dict. See example.
        :type issue_list: list
        :type issue_dict: dict
        :example:
            issue_dict = {
                'project': {'key': 'CHR'},
                'summary': 'New issue from jira-python',
                'description': 'Look into this one',
                'issuetype': {'name': 'Chairs'},
            }

        :return: Result of attempt to create issues.
        :rtype: list of dict with status at top level.
        """
        issues = await self.jira.create_issues(field_list=issue_list)
        return issues

    async def comment_on_issue(self, issue: object, comment: str):
        """Comment on Jira issue.

        :param issue:
            issue object or JIRA Key that can be retrieved by calling get_issue
        :type issue: object


        :param comment: Comment to be added to issue, defaults to None
        :type comment: str

        :return: comment that has been created
        :rtype: object
        """
        return await self.jira.add_comment(issue, comment)

    async def delete_comment(self, comment: object):
        """Delete an existing comment"""
        return comment.delete()

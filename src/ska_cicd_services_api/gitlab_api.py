# -*- coding: utf-8 -*-
import os

import aiohttp
import gidgetlab.aiohttp


class GitLabApi:
    """
    A class used to interact with GitLab Rest API with
    the help of library gidgetlab.aiohttp

    To use it, you need create aiohttp session and pass it to the constructor.

    Example Usage:
    async with aiohttp.ClientSession() as session:
        api = gitlab_api.GitLabApi(session)
        # Make calls, save responses, iterate over values with async for...

    For more information about the data returned by this class, please refer
    https://docs.gitlab.com/ee/api/

    """

    def __init__(
        self,
        session: aiohttp.ClientSession,
        api_base="https://gitlab.com",
        gitlab_api_requester: str = "",
        gitlab_api_private_token: str = "",
        cache=None,
    ):
        """Save authentication parameters for the API connection

        Args:
            gitlab_api_requester ([type], optional):
                the requester for the interaction with Gitlab
                (will be taken from the environment variable
                REQUESTER if not defined on method call).
                Defaults to None.
            gitlab_api_private_token ([type], optional):
                the access token for the interaction with Gitlab
                (will be taken from the environment variable
                PRIVATE_TOKEN if not defined on method call).
                Defaults to None.
        """
        self.gl = gidgetlab.aiohttp.GitLabAPI(
            session=session,
            requester=os.getenv("GITLAB_API_REQUESTER", gitlab_api_requester),
            url=api_base,
            access_token=os.getenv(
                "GITLAB_API_PRIVATE_TOKEN", gitlab_api_private_token
            ),
            cache=cache,
        )

        self._session = session
        self._private_token = gitlab_api_private_token

    async def get_commits(self, proj_id, branch):
        """Get the current authenticated user info.

        :param: proj_id: the project id
        :param: branch: the branch to get commits from

        :return: https://docs.gitlab.com/ee/api/commits.html#list-repository-commits # NOQA: E501
        """
        url = f"/projects/{proj_id}/repository/commits?ref={branch}"
        return self.gl.getiter(url)

    async def get_commit(self, proj_id, commit_sha):
        """Get the current authenticated user info.

        :param: proj_id: the project id
        :param: branch: the branch to get commits from

        :return: https://docs.gitlab.com/ee/api/commits.html#list-repository-commits # NOQA: E501
        """
        url = f"/projects/{proj_id}/repository/commits/{commit_sha}"
        return await self.gl.getitem(url)

    async def get_current_user(self, access_token=None):
        """Get the current authenticated user info.

        :param: access_token: the access token of a user to get the info of

        :return: https://docs.gitlab.com/ee/api/users.html#list-current-user-for-normal-users # NOQA: E501
        """
        url = "/user"

        # ST-1030: Useful for use with OAuth2 for getting the user's ID
        if access_token is not None:
            url = f"{url}?access_token={access_token}"

        return await self.gl.getitem(url)

    async def get_commit_diffs(self, proj_id, commit_id):
        """Get git diffs of a commit in a project.

        :param: proj_id: the project id of the commit

        :param: commit_id: the commit hash

        :return: https://docs.gitlab.com/ee/api/commits.html#get-the-diff-of-a-commit # NOQA: E501

        """
        url = f"/projects/{proj_id}/repository/commits/{commit_id}/diff"
        return self.gl.getiter(url)

    async def add_comment(self, proj_id, mr_id, comment):
        """Add a comment to a merge request

        :param: proj_id: the project id of the merge request

        :param: mr_id: the merge request iid

        :param: comment: The comment to be added in the merge request

        :return: https://docs.gitlab.com/ee/api/notes.html#create-new-merge-request-note # NOQA: E501

        """
        url = f"/projects/{proj_id}/merge_requests/{mr_id}/notes"
        return await self.gl.post(url, data={"body": comment})

    async def remove_comment(self, proj_id, mr_id, comment_id):
        """Remove a comment from a merge request

        :param: proj_id: the project id of the merge request

        :param: mr_id: the merge request iid

        :param: mr_id: The comment to be removed in the merge request

        :return: https://docs.gitlab.com/ee/api/notes.html#delete-a-merge-request-note # NOQA: E501

        """
        url = f"/projects/{proj_id}/merge_requests/{mr_id}/notes/{comment_id}"
        return await self.gl.delete(url)

    async def get_merge_request_info(self, proj_id, mr_id):
        """Get merge request information

        :param: proj_id: the project id of the merge request

        :param: mr_id: the merge request iid

        :return: https://docs.gitlab.com/ee/api/merge_requests.html#get-single-mr # NOQA: E501

        """
        url = f"/projects/{proj_id}/merge_requests/{mr_id}"
        return await self.gl.getitem(url)

    async def get_merge_request_approvals_info(self, proj_id, mr_id):
        """Get merge request approvals

        :param: proj_id: the project id of the merge request

        :param: mr_id: the merge request iid

        :return: https://docs.gitlab.com/ee/api/merge_request_approvals.html#merge-request-level-mr-approvals # NOQA: E501

        """
        url = f"/projects/{proj_id}/merge_requests/{mr_id}/approvals"
        return await self.gl.getitem(url)

    async def add_approval_merge_request(self, proj_id, mr_id):
        """Approve Merge Request

        :param: proj_id: the project id of the merge request

        :param: mr_id: the merge request iid

        :return: https://docs.gitlab.com/ee/api/merge_request_approvals.html#approve-merge-request # NOQA: E501

        """

        url = f"/projects/{proj_id}/merge_requests/{mr_id}/approve"
        return await self.gl.post(url, data={})

    async def remove_approval_merge_request(self, proj_id, mr_id):
        """Unapprove Merge Request

        :param: proj_id: the project id of the merge request

        :param: mr_id: the merge request iid

        :return: https://docs.gitlab.com/ee/api/merge_request_approvals.html#unapprove-merge-request # NOQA: E501

        """

        url = f"/projects/{proj_id}/merge_requests/{mr_id}/unapprove"
        return await self.gl.post(url, data={})

    async def get_project_merge_request_approvals_info(self, proj_id):
        """Get project level merge request approvals settings

        :param: proj_id: the project id of the merge request

        :return: https://docs.gitlab.com/ee/api/merge_request_approvals.html#merge-request-level-mr-approvals # NOQA: E501

        """
        url = f"/projects/{proj_id}/approvals"
        return await self.gl.getitem(url)

    async def get_merge_request_commits(self, proj_id, mr_id):
        """Get all commits in a merge request

        :param: proj_id: the project id of the merge request

        :param: mr_id: the merge request iid

        :return: https://docs.gitlab.com/ee/api/merge_requests.html#get-single-mr-commits # NOQA: E501

        """
        url = f"/projects/{proj_id}/merge_requests/{mr_id}/commits"
        return self.gl.getiter(url)

    async def get_project_settings_info(self, proj_id, with_license=False):
        """Get project settings information

        :param: proj_id: the project id of the merge request
        :param: with_license: filter with license
        :return: https://docs.gitlab.com/ee/api/projects.html#get-single-project # NOQA: E501

        """
        url = f"/projects/{proj_id}?license={with_license}"
        return await self.gl.getitem(url)

    async def get_project_members(self, proj_id):
        """Get project members

        :param: proj_id: the project id of the merge request

        :return: https://docs.gitlab.com/ee/api/members.html#list-all-members-of-a-group-or-project # NOQA: E501

        """
        url = f"/projects/{proj_id}/members"
        return self.gl.getiter(url)

    async def get_merge_request_comments(self, proj_id, mr_id):
        """Get list of merge request comments

        :param: proj_id: the project id of the merge request

        :param: mr_id: the merge request iid

        :return: https://docs.gitlab.com/ee/api/notes.html#list-all-merge-request-notes # NOQA: E501

        """
        url = f"/projects/{proj_id}/merge_requests/{mr_id}/notes"
        return self.gl.getiter(url)

    async def get_comment_info(self, proj_id, mr_id, comment_id):
        """Get a single comment information from a merge request

        :param: proj_id: the project id of the merge request

        :param: mr_id: the merge request iid

        :param: comment_id: the merge request comment id

        :return: https://docs.gitlab.com/ee/api/notes.html#get-single-merge-request-note # NOQA: E501

        """
        url = f"/projects/{proj_id}/merge_requests/{mr_id}/notes/{comment_id}"
        return await self.gl.getitem(url)

    async def get_job_token_scope(self, proj_id):
        """Fetch the CI/CD job token access settings (job token scope) of a project.

        :param: proj_id: the project id of the merge request

        :return: https://docs.gitlab.com/ee/api/project_job_token_scopes.html # NOQA: E501

        """
        url = f"/projects/{proj_id}/job_token_scope"
        return await self.gl.getitem(url)

    async def modify_comment(self, proj_id, mr_id, comment_id, comment):
        """Update merge request comment

        :param: proj_id: the project id of the merge request

        :param: mr_id: the merge request iid

        :param: comment_id: the merge request comment id

        :param: comment: the merge request comment

        :return: https://docs.gitlab.com/ee/api/notes.html#modify-existing-merge-request-note # NOQA: E501

        """
        url = f"/projects/{proj_id}/merge_requests/{mr_id}/notes/{comment_id}"
        return await self.gl.put(url, data={"body": comment})

    async def modify_merge_request(  # pylint: disable=too-many-arguments
        self,
        proj_id,
        mr_id,
        target_branch=None,
        title=None,
        assignee_id=None,
        assignee_ids=None,
        reviewer_ids=None,
        milestone_id=None,
        labels=None,
        add_labels=None,
        remove_labels=None,
        description=None,
        state_event=None,
        remove_source_branch=None,
        squash=None,
        discussion_locked=None,
        allow_collaboration=None,
    ):
        """Update merge request

        :param: proj_id: the project id of the merge request

        :param: mr_id: the merge request iid

        :param: target_branch: The target branch

        :param: title: Title of MR

        :param: assignee_id: The ID of the user to assign the merge request to. Set to 0 or provide an empty value to unassign all assignees # NOQA: E501

        :param: assignee_ids: The ID of the user(s) to assign the MR to. Set to 0 or provide an empty value to unassign all assignees # NOQA: E501

        :param: reviewer_ids: The ID of the user(s) set as a reviewer to the MR. Set the value to 0 or provide an empty value to unset all reviewers # NOQA: E501

        :param: milestone_id: The global ID of a milestone to assign the merge request to. Set to 0 or provide an empty value to unassign a milestone # NOQA: E501

        :param: labels: Comma-separated label names for a merge request. Set to an empty string to unassign all labels # NOQA: E501

        :param: add_labels: Comma-separated label names to add to a merge request # NOQA: E501

        :param: remove_labels: Comma-separated label names to remove from a merge request # NOQA: E501

        :param: description: Description of MR. Limited to 1,048,576 characters

        :param: state_event: New state (close/reopen)

        :param: remove_source_branch: Flag indicating if a merge request should remove the source branch when merging # NOQA: E501

        :param: squash: Squash commits into a single commit when merging

        :param: discussion_locked: Flag indicating if the merge request's discussion is locked. If the discussion is locked only project members can add, edit or resolve comments # NOQA: E501

        :param: allow_collaboration: Allow commits from members who can merge to the target branch # NOQA: E501

        :return: https://docs.gitlab.com/ee/api/merge_requests.html#update-mr # NOQA: E501

        """
        settings = {
            "target_branch": target_branch,
            "title": title,
            "assignee_id": assignee_id,
            "assignee_ids": assignee_ids,
            "reviewer_ids": reviewer_ids,
            "milestone_id": milestone_id,
            "labels": labels,
            "add_labels": add_labels,
            "remove_labels": remove_labels,
            "description": description,
            "state_event": state_event,
            "remove_source_branch": remove_source_branch,
            "squash": squash,
            "discussion_locked": discussion_locked,
            "allow_collaboration": allow_collaboration,
        }

        # ST-1030: Strip all None keys from the input data
        settings = {
            key: value for key, value in settings.items() if value is not None
        }

        url = f"/projects/{proj_id}/merge_requests/{mr_id}"
        return await self.gl.put(url, data=settings)

    async def modify_project_level_mr_approval_settings(
        self,
        proj_id,
        approvals_before_merge=None,
        reset_approvals_on_push=None,
        disable_overriding_approvers_per_merge_request=None,
        merge_requests_author_approval=None,
        merge_requests_disable_committers_approval=None,
        require_password_to_approve=None,
    ):
        """Update project-level merge request approval settings

        :param: proj_id: the project id of the merge request

        :param: approvals_before_merge: How many approvals are required before an MR can be merged. Deprecated in 12.0 in favor of Approval Rules API # NOQA: E501

        :param: reset_approvals_on_push: Reset approvals on a new push

        :param: disable_overriding_approvers_per_merge_request: Allow/Disallow overriding approvers per MR # NOQA: E501

        :param: merge_requests_author_approval: Allow/Disallow authors from self approving merge requests; true means authors can self approve # NOQA: E501

        :param: merge_requests_disable_committers_approval: Allow/Disallow committers from self approving merge requests # NOQA: E501

        :param: require_password_to_approve: Require approver to enter a password to authenticate before adding the approval # NOQA: E501

        :return: https://docs.gitlab.com/ee/api/merge_request_approvals.html#change-configuration # NOQA: E501

        """
        settings = {
            "approvals_before_merge": approvals_before_merge,
            "reset_approvals_on_push": reset_approvals_on_push,
            "disable_overriding_approvers_per_merge_request": disable_overriding_approvers_per_merge_request,  # NOQA: E501
            "merge_requests_author_approval": merge_requests_author_approval,
            "merge_requests_disable_committers_approval": merge_requests_disable_committers_approval,  # NOQA: E501
            "require_password_to_approve": require_password_to_approve,
        }

        # ST-1030: Strip all None keys from the input data
        settings = {
            key: value for key, value in settings.items() if value is not None
        }

        url = f"/projects/{proj_id}/approvals"
        return await self.gl.post(url, data=settings)

    async def modify_mr_level_mr_approval_settings(
        self,
        proj_id,
        mr_id,
        approvals_required,
    ):
        """Update mr-level merge request approval settings

        :param: proj_id: the project id of the merge request

        :param: mr_id: the merge request iid

        :param: approvals_required: Approvals required before MR can be merged. Deprecated in 12.0 in favor of Approval Rules API # NOQA: E501

        :return: https://docs.gitlab.com/ee/api/merge_request_approvals.html#change-approval-configuration # NOQA: E501

        """
        settings = {
            "approvals_required": approvals_required,
        }

        url = f"/projects/{proj_id}/merge_requests/{mr_id}/approvals"
        return await self.gl.post(url, data=settings)

    async def modify_project(  # pylint: disable = too-many-arguments,too-many-locals # NOQA: E501
        self,
        proj_id,
        allow_merge_on_skipped_pipeline=None,
        analytics_access_level=None,
        approvals_before_merge=None,
        auto_cancel_pending_pipelines=None,
        auto_devops_deploy_strategy=None,
        auto_devops_enabled=None,
        autoclose_referenced_issues=None,
        avatar=None,
        build_coverage_regex=None,
        build_git_strategy=None,
        build_timeout=None,
        builds_access_level=None,
        ci_config_path=None,
        ci_default_git_depth=None,
        ci_forward_deployment_enabled=None,
        container_expiration_policy_attributes=None,
        container_registry_enabled=None,
        container_registry_access_level=None,
        default_branch=None,
        description=None,
        emails_disabled=None,
        external_authorization_classification_label=None,
        forking_access_level=None,
        import_url=None,
        issues_access_level=None,
        issues_enabled=None,
        jobs_enabled=None,
        lfs_enabled=None,
        merge_commit_template=None,
        merge_method=None,
        merge_pipelines_enabled=None,
        merge_requests_access_level=None,
        merge_requests_enabled=None,
        merge_trains_enabled=None,
        mirror_overwrites_diverged_branches=None,
        mirror_trigger_builds=None,
        mirror_user_id=None,
        mirror=None,
        name=None,
        operations_access_level=None,
        only_allow_merge_if_all_discussions_are_resolved=None,
        only_allow_merge_if_pipeline_succeeds=None,
        only_mirror_protected_branches=None,
        packages_enabled=None,
        pages_access_level=None,
        requirements_access_level=None,
        restrict_user_defined_variables=None,
        path=None,
        printing_merge_request_link_enabled=None,
        public_builds=None,
        remove_source_branch_after_merge=None,
        repository_access_level=None,
        repository_storage=None,
        request_access_enabled=None,
        resolve_outdated_diff_discussions=None,
        security_and_compliance_access_level=None,
        service_desk_enabled=None,
        shared_runners_enabled=None,
        show_default_award_emojis=None,
        snippets_access_level=None,
        snippets_enabled=None,
        squash_commit_template=None,
        squash_option=None,
        suggestion_commit_message=None,
        tag_list=None,
        topics=None,
        visibility=None,
        wiki_access_level=None,
        wiki_enabled=None,
        issues_template=None,
        merge_requests_template=None,
        keep_latest_artifact=None,
    ):
        """Edit project settings

        :param: proj_id: the project id of the merge request

        :param: allow_merge_on_skipped_pipeline: Set whether or not merge requests can be merged with skipped jobs. # NOQA: E501

        :param: analytics_access_level: One of disabled, private or enabled

        :param: approvals_before_merge: How many approvers should approve merge request by default. To configure approval rules, see Merge request approvals API. # NOQA: E501

        :param: auto_cancel_pending_pipelines: Auto-cancel pending pipelines. This isn't a boolean, but enabled/disabled. # NOQA: E501

        :param: auto_devops_deploy_strategy: Auto Deploy strategy (continuous, manual, or timed_incremental). # NOQA: E501

        :param: auto_devops_enabled: Enable Auto DevOps for this project.

        :param: autoclose_referenced_issues: Set whether auto-closing referenced issues on default branch. # NOQA: E501

        :param: avatar: Image file for avatar of the project.

        :param: build_coverage_regex: Test coverage parsing.

        :param: build_git_strategy: The Git strategy. Defaults to fetch.

        :param: build_timeout: The maximum amount of time, in seconds, that a job can run. # NOQA: E501

        :param: builds_access_level: One of disabled, private, or enabled.

        :param: ci_config_path: The path to CI configuration file.

        :param: ci_default_git_depth: Default number of revisions for shallow cloning. # NOQA: E501

        :param: ci_forward_deployment_enabled: When a new deployment job starts, skip older deployment jobs that are still pending # NOQA: E501

        :param: container_expiration_policy_attributes: Update the image cleanup policy for this project. Accepts: cadence (string), keep_n (integer), older_than (string), name_regex (string), name_regex_delete (string), name_regex_keep (string), enabled (boolean). # NOQA: E501

        :param: container_registry_enabled: Deprecated) Enable container registry for this project. Use container_registry_access_level instead. # NOQA: E501

        :param: container_registry_access_level: Set visibility of container registry, for this project, to one of disabled, private or enabled. # NOQA: E501

        :param: default_branch: The default branch name.

        :param: description: Short project description.

        :param: emails_disabled: Disable email notifications.

        :param: external_authorization_classification_label: The classification label for the project. # NOQA: E501

        :param: forking_access_level: One of disabled, private, or enabled.

        :param: import_url: URL to import repository from.

        :param: issues_access_level: One of disabled, private, or enabled.

        :param: issues_enabled: Deprecated) Enable issues for this project. Use issues_access_level instead. # NOQA: E501

        :param: jobs_enabled: Deprecated) Enable jobs for this project. Use builds_access_level instead. # NOQA: E501

        :param: lfs_enabled: Enable LFS.

        :param: merge_commit_template: Template used to create merge commit message in merge requests. (Introduced in GitLab 14.5.) # NOQA: E501

        :param: merge_method: Set the merge method used.

        :param: merge_pipelines_enabled: Enable or disable merge pipelines.

        :param: merge_requests_access_level: One of disabled, private, or enabled. # NOQA: E501

        :param: merge_requests_enabled: Deprecated) Enable merge requests for this project. Use merge_requests_access_level instead. # NOQA: E501

        :param: merge_trains_enabled: Enable or disable merge trains.

        :param: mirror_overwrites_diverged_branches: Pull mirror overwrites diverged branches. # NOQA: E501

        :param: mirror_trigger_builds: Pull mirroring triggers builds.

        :param: mirror_user_id: User responsible for all the activity surrounding a pull mirror event. (administrators only) # NOQA: E501

        :param: mirror: Enables pull mirroring in a project.

        :param: name: The name of the project.

        :param: operations_access_level: One of disabled, private, or enabled.

        :param: only_allow_merge_if_all_discussions_are_resolved: Set whether merge requests can only be merged when all the discussions are resolved. # NOQA: E501

        :param: only_allow_merge_if_pipeline_succeeds: Set whether merge requests can only be merged with successful jobs. # NOQA: E501

        :param: only_mirror_protected_branches: Only mirror protected branches.

        :param: packages_enabled: Enable or disable packages repository feature. # NOQA: E501

        :param: pages_access_level: One of disabled, private, enabled, or public. # NOQA: E501

        :param: requirements_access_level: One of disabled, private, enabled or public # NOQA: E501

        :param: restrict_user_defined_variables: Allow only users with the Maintainer role to pass user-defined variables when triggering a pipeline. For example when the pipeline is triggered in the UI, with the API, or by a trigger token. # NOQA: E501

        :param: path: Custom repository name for the project. By default generated based on name. # NOQA: E501

        :param: printing_merge_request_link_enabled: Show link to create/view merge request when pushing from the command line. # NOQA: E501

        :param: public_builds: If true, jobs can be viewed by non-project members. # NOQA: E501

        :param: remove_source_branch_after_merge: Enable Delete source branch option by default for all new merge requests. # NOQA: E501

        :param: repository_access_level: One of disabled, private, or enabled.

        :param: repository_storage: Which storage shard the repository is on. (administrators only) # NOQA: E501

        :param: request_access_enabled: Allow users to request member access.

        :param: resolve_outdated_diff_discussions: Automatically resolve merge request diffs discussions on lines changed with a push. # NOQA: E501

        :param: security_and_compliance_access_level: Set visibility of security and compliance. # NOQA: E501

        :param: service_desk_enabled: Enable or disable Service Desk feature.

        :param: shared_runners_enabled: Enable shared runners for this project.

        :param: show_default_award_emojis: Show default award emojis.

        :param: snippets_access_level: One of disabled, private, or enabled.

        :param: snippets_enabled: Deprecated) Enable snippets for this project. Use snippets_access_level instead. # NOQA: E501

        :param: squash_commit_template: Template used to create squash commit message in merge requests. (Introduced in GitLab 14.6.) # NOQA: E501

        :param: squash_option: One of never, always, default_on, or default_off. # NOQA: E501

        :param: suggestion_commit_message: The commit message used to apply merge request suggestions. # NOQA: E501

        :param: tag_list: Deprecated in GitLab 14.0) The list of tags for a project; put array of tags, that should be finally assigned to a project. Use topics instead. # NOQA: E501

        :param: topics: The list of topics for the project. This replaces any existing topics that are already added to the project. (Introduced in GitLab 14.0.) # NOQA: E501

        :param: visibility: See project visibility level.

        :param: wiki_access_level: One of disabled, private, or enabled.

        :param: wiki_enabled: Deprecated) Enable wiki for this project. Use wiki_access_level instead. # NOQA: E501

        :param: issues_template: Default description for Issues. Description is parsed with GitLab Flavored Markdown. See Templates for issues and merge requests. # NOQA: E501

        :param: merge_requests_template: Default description for Merge Requests. Description is parsed with GitLab Flavored Markdown. See Templates for issues and merge requests. # NOQA: E501

        :param: keep_latest_artifact: Disable or enable the ability to keep the latest artifact for this project. # NOQA: E501

        :return: https://docs.gitlab.com/ee/api/projects.html#edit-project # NOQA: E501

        """
        settings = {
            "allow_merge_on_skipped_pipeline": allow_merge_on_skipped_pipeline,
            "analytics_access_level": analytics_access_level,
            "approvals_before_merge": approvals_before_merge,
            "auto_cancel_pending_pipelines": auto_cancel_pending_pipelines,
            "auto_devops_deploy_strategy": auto_devops_deploy_strategy,
            "auto_devops_enabled": auto_devops_enabled,
            "autoclose_referenced_issues": autoclose_referenced_issues,
            "avatar": avatar,
            "build_coverage_regex": build_coverage_regex,
            "build_git_strategy": build_git_strategy,
            "build_timeout": build_timeout,
            "builds_access_level": builds_access_level,
            "ci_config_path": ci_config_path,
            "ci_default_git_depth": ci_default_git_depth,
            "ci_forward_deployment_enabled": ci_forward_deployment_enabled,
            "container_expiration_policy_attributes": container_expiration_policy_attributes,  # NOQA: E501
            "container_registry_enabled": container_registry_enabled,
            "container_registry_access_level": container_registry_access_level,
            "default_branch": default_branch,
            "description": description,
            "emails_disabled": emails_disabled,
            "external_authorization_classification_label": external_authorization_classification_label,  # NOQA: E501
            "forking_access_level": forking_access_level,
            "import_url": import_url,
            "issues_access_level": issues_access_level,
            "issues_enabled": issues_enabled,
            "jobs_enabled": jobs_enabled,
            "lfs_enabled": lfs_enabled,
            "merge_commit_template": merge_commit_template,
            "merge_method": merge_method,
            "merge_pipelines_enabled": merge_pipelines_enabled,
            "merge_requests_access_level": merge_requests_access_level,
            "merge_requests_enabled": merge_requests_enabled,
            "merge_trains_enabled": merge_trains_enabled,
            "mirror_overwrites_diverged_branches": mirror_overwrites_diverged_branches,  # NOQA: E501
            "mirror_trigger_builds": mirror_trigger_builds,
            "mirror_user_id": mirror_user_id,
            "mirror": mirror,
            "name": name,
            "operations_access_level": operations_access_level,
            "only_allow_merge_if_all_discussions_are_resolved": only_allow_merge_if_all_discussions_are_resolved,  # NOQA: E501
            "only_allow_merge_if_pipeline_succeeds": only_allow_merge_if_pipeline_succeeds,  # NOQA: E501
            "only_mirror_protected_branches": only_mirror_protected_branches,
            "packages_enabled": packages_enabled,
            "pages_access_level": pages_access_level,
            "requirements_access_level": requirements_access_level,
            "restrict_user_defined_variables": restrict_user_defined_variables,
            "path": path,
            "printing_merge_request_link_enabled": printing_merge_request_link_enabled,  # NOQA: E501
            "public_builds": public_builds,
            "remove_source_branch_after_merge": remove_source_branch_after_merge,  # NOQA: E501
            "repository_access_level": repository_access_level,
            "repository_storage": repository_storage,
            "request_access_enabled": request_access_enabled,
            "resolve_outdated_diff_discussions": resolve_outdated_diff_discussions,  # NOQA: E501
            "security_and_compliance_access_level": security_and_compliance_access_level,  # NOQA: E501
            "service_desk_enabled": service_desk_enabled,
            "shared_runners_enabled": shared_runners_enabled,
            "show_default_award_emojis": show_default_award_emojis,
            "snippets_access_level": snippets_access_level,
            "snippets_enabled": snippets_enabled,
            "squash_commit_template": squash_commit_template,
            "squash_option": squash_option,
            "suggestion_commit_message": suggestion_commit_message,
            "tag_list": tag_list,
            "topics": topics,
            "visibility": visibility,
            "wiki_access_level": wiki_access_level,
            "wiki_enabled": wiki_enabled,
            "issues_template": issues_template,
            "merge_requests_template": merge_requests_template,
            "keep_latest_artifact": keep_latest_artifact,
        }

        # ST-1030: Strip all None keys from the input data
        settings = {
            key: value for key, value in settings.items() if value is not None
        }

        url = f"/projects/{proj_id}"
        return await self.gl.put(url, data=settings)

    async def get_search_results(self, proj_id, scope, search, ref=""):
        """Search a string in project

        :param: proj_id: the project id of the merge request

        :param: scope: the scope to search in

        :param: search: the search query

        :return: https://docs.gitlab.com/ee/api/search.html#project-search-api

        """
        url = (
            f"/projects/{proj_id}/search?scope={scope}"
            f"&ref={ref}&search={search}"
        )
        return self.gl.getiter(url)

    async def get_job_artifacts(self, proj_id, job_id):
        """Get the job’s artifacts zipped archive of a project.

        :param: proj_id: the project id of the merge request

        :param: job_id: ID of a job.


        :return: https://docs.gitlab.com/ee/api/job_artifacts.html#get-job-artifacts # NOQA: E501

        """

        url = f"https://gitlab.com/api/v4/projects/{proj_id}/jobs/{job_id}/artifacts"  # NOQA: E501

        self._session.headers["PRIVATE-TOKEN"] = self._private_token

        async with self._session.get(url) as resp:
            return await resp.read()

    async def get_file_from_repository(self, proj_id, file_path, ref):
        """Get files in project

        :param: proj_id: the project id of the merge request

        :param: file_path: the URL encoded full path to new file. Ex. lib%2Fclass%2Erb

        :param: ref: the name of branch, tag or commit

        :return: https://docs.gitlab.com/ee/api/repository_files.html#get-file-from-repository # NOQA: E501

        """
        url = f"/projects/{proj_id}/repository/files/{file_path}?ref={ref}"
        return await self.gl.getitem(url)

    async def get_repo_tree(
        self,
        proj_id,
        ref="master",
        path="/",
        recursive=False,
        per_page=20,
        pagination="keyset",
        page_token="",
    ):
        """List of repository files and directories in a project

        :param: proj_id: the project id of the merge request

        :return: https://docs.gitlab.com/ee/api/repositories.html#list-repository-tree # NOQA: E501

        """
        url = f"/projects/{proj_id}/repository/tree?ref={ref}&path={path}&recursive={recursive}&per_page={per_page}&pagination={pagination}&page_token={page_token}"  # NOQA: E501
        return self.gl.getiter(url)

    async def get_mr_diff_versions(self, proj_id, mr_id):
        """Get a list of merge request diff versions

        :param: proj_id: the project id of the merge request

        :param: mr_id: the merge request iid

        :return: https://docs.gitlab.com/ee/api/merge_requests.html#get-mr-diff-versions # NOQA: E501

        """
        url = f"/projects/{proj_id}/merge_requests/{mr_id}/versions"
        return self.gl.getiter(url)

    async def get_single_single_mr_diff_version(
        self, proj_id, mr_id, version_id
    ):
        """Get a single merge request diff version

        :param: proj_id: the project id of the merge request

        :param: mr_id: the merge request iid

        :param: version_id: the version id

        :return: https://docs.gitlab.com/ee/api/merge_requests.html#get-a-single-mr-diff-version # NOQA: E501

        """
        url = (
            f"/projects/{proj_id}/merge_requests/{mr_id}/versions/{version_id}"
        )
        return await self.gl.getitem(url)

    async def get_project_hooks(self, proj_id):
        """Get a list of project hooks.

        :param: proj_id: the project id of the merge request

        :return: https://docs.gitlab.com/ee/api/projects.html#list-project-hooks # NOQA: E501

        """
        url = f"/projects/{proj_id}/hooks"
        return self.gl.getiter(url)

    async def create_mr(  # pylint: disable=too-many-arguments
        self,
        proj_id,
        source_branch,
        target_branch,
        title,
        assignee_id=None,
        assignee_ids=None,
        reviewer_ids=None,
        description="",
        target_project_id=None,
        labels="",
        milestone_id=None,
        remove_source_branch=True,
        allow_collaboration=True,
        squash=False,
    ):
        """Create a merge request.

        :param: proj_id: the project id for the merge request

        :param: source_branch: the source branch of the merge request

        :param: target_branch: the target branch of the merge request

        :param: title: the title of the merge request

        :param: assignee_id: Assignee user ID.

        :param: assignee_ids: array with user(s) ids to assign

        :param: reviewer_ids: array with user(s) ids to add as reviewers

        :param: description: the description of the merge request

        :param: target_project_id: The target project (numeric ID).

        :param: labels: A comma separated string with labels

        :param: milestone_id: The global ID of a milestone.

        :param: remove_source_branch: Should source branch be deleted when merging  # NOQA: E501

        :param: allow_collaboration: Allow commits from members who can merge to the target branch. # NOQA: E501

        :param: squash: Should commits be squashed when merging

        :return: https://docs.gitlab.com/ee/api/merge_requests.html#create-mr

        """

        data = {
            "source_branch": source_branch,
            "target_branch": target_branch,
            "title": title,
            "assignee_id": assignee_id,
            "assignee_ids": assignee_ids,
            "reviewer_ids": reviewer_ids,
            "description": description,
            "target_project_id": target_project_id,
            "labels": labels,
            "milestone_id": milestone_id,
            "remove_source_branch": remove_source_branch,
            "allow_collaboration": allow_collaboration,
            "squash": squash,
        }
        url = f"/projects/{proj_id}/merge_requests"
        return await self.gl.post(url, data=data)

    async def create_branch(self, proj_id, branch, ref="master"):
        """Create a merge request.

        :param: proj_id: the project id for the merge request

        :param: branch: the name of the branch

        :param: ref: branch name to create the branch from

        :return: https://docs.gitlab.com/ee/api/branches.html#create-repository-branch # NOQA: E501

        """

        url = f"/projects/{proj_id}/repository/branches"
        return await self.gl.post(url, data={"branch": branch, "ref": ref})

    async def get_merge_requests(self, proj_id, mr_title, state="opened"):
        """check if a mr for the project exists with this title

        :param: proj_id: the project id of the merge request

        :param: mr_title: the merge request title to check

        :param: state: ...

        :return: https://docs.gitlab.com/ee/api/merge_requests.html#list-merge-requests # NOQA: E501

        """
        url = f"/projects/{proj_id}/merge_requests?scope=all&state={state}&search={mr_title}&in=title"  # NOQA: E501
        return self.gl.getiter(url)

    async def delete_branch(self, proj_id, branch):
        """Delete a Branch.

        :param: proj_id: the project id for the merge request

        :param: branch: the name of the branch

        :return: https://docs.gitlab.com/ee/api/branches.html#delete-repository-branch # NOQA: E501

        """

        url = f"/projects/{proj_id}/repository/branches/{branch}"
        return await self.gl.delete(url)

    async def trigger_scanning(self, proj_id, cont_image):
        """trigger a pipeline for vulnerability scanning.

        :param: proj_id: the project id for the merge request

        :cont_image: the full name of the artefact, including CAR

        :return:  https://docs.gitlab.com/ee/api/pipelines.html#create-a-new-pipeline  # NOQA: E501

        """

        url = f"/projects/{proj_id}/"
        resul = await self.gl.getitem(url)
        ref = resul["default_branch"]

        variables = {
            "key": "FULL_IMAGE_NAME",
            "variable type": "file",
            "value": cont_image,
        }

        url = f"/projects/{proj_id}/pipeline?ref={ref}"
        return await self.gl.post(url, data=variables)

    async def get_status_checks(self, proj_id, mr_id):
        """Get the status checks for a given project and merge request

        :param: proj_id: the project id for the merge request

        :mr_id: the merge request ID

        :return:  https://docs.gitlab.com/ee/api/status_checks.html#list-status-checks-for-a-merge-request  # NOQA: E501

        """
        url = f"/projects/{proj_id}/merge_requests/{mr_id}/status_checks"
        return await self.gl.getitem(url)

    async def set_status_check(
        self, proj_id, mr_id, sha, external_status_check_id, status
    ):
        """Set the status checks for a given project and merge request

        :param: proj_id: the project id for the merge request

        :mr_id: the merge request ID

        :sha: the head commit of the merge request

        :external_status_check_id: the id of the status check to modify

        :status: the new status ("pending", "passed" or "failed)

        :return:  https://docs.gitlab.com/ee/api/status_checks.html#set-status-of-an-external-status-check  # NOQA: E501

        """
        url = f"/projects/{proj_id}/merge_requests/{mr_id}/status_check_responses"  # NOQA: E501

        variables = {
            "sha": sha,
            "external_status_check_id": external_status_check_id,
            "status": status,
        }

        return await self.gl.post(url, data=variables)

    async def modify_job_token_access(
        self,
        proj_id,
        enabled=None,
    ):
        """
        Update a protected environment.

        :param proj_id: The project id for the merge request.
        :param enabled: Indicates if the Limit access to this project setting should be enabled.  # NOQA: E501
        :return: https://docs.gitlab.com/ee/api/project_job_token_scopes.html  # NOQA: E501
        """

        settings = {
            "enabled": enabled,
        }

        settings = {
            key: value for key, value in settings.items() if value is not None
        }

        url = f"/projects/{proj_id}/job_token_scope"
        return await self.gl.patch(url, data=settings)

import os
from typing import List, Optional

from harborapi import HarborAsyncClient
from harborapi.exceptions import Conflict as _Conflict
from harborapi.exceptions import NotFound as _NotFound
from harborapi.models import (
    Artifact,
    HarborVulnerabilityReport,
    Label,
    Repository,
    Tag,
)


class Conflict(_Conflict):
    pass


class NotFound(_NotFound):
    pass


class HarborApi:
    def __init__(
        self,
        harbor_url: str = None,
        username: str = None,
        password: str = None,
    ) -> None:
        harbor_url = (
            harbor_url if harbor_url is not None else os.getenv("HARBOR_URL")
        )
        username = (
            username if username is not None else os.getenv("HARBOR_USERNAME")
        )
        password = (
            password if password is not None else os.getenv("HARBOR_PASSWORD")
        )

        self.client = HarborAsyncClient(
            url=harbor_url,
            username=username,
            secret=password,
        )

    async def get_repository(
        self, project_name: str, repository_name: str
    ) -> Repository:
        """Get a repository.

        Parameters
        ----------
        project_name : str
            The name of the project the repository belongs to.
        repository_name : str
            The name of the repository.

        Returns
        -------
        Repository
            The repository.
        """
        repo = await self.client.get_repository(project_name, repository_name)
        return repo

    async def delete_repository(
        self, project_name: str, repository_name: str
    ) -> None:
        """Delete a repository.

        Parameters
        ----------
        project_name : str
            The name of the project the repository belongs to.
        repository_name : str
            The name of the repository.
        """
        await self.client.delete_repository(project_name, repository_name)

    async def get_repositories(
        self,
        project_name: str,
        page: Optional[int] = 1,
        page_size: Optional[int] = 20,
    ) -> list[Repository]:
        """Get a list of all repositories, optionally only in a specific project. # NOQA: E501

        Parameters
        ----------
        project_name : Optional[str]
            The name of the project to retrieve repositories from.
            If None, retrieve repositories from all projects.
        page : int
            The page of results to return
        page_size : int
            The number of results to return per page

        Returns
        -------
        List[Repository]
            A list of repositories matching the query.
        """
        repos = await self.client.get_repositories(
            project_name, page=page, page_size=page_size
        )
        return repos

    async def get_artifact(
        self,
        project_name: str,
        repository_name: str,
        reference: str,
        page: Optional[int] = 1,
        page_size: Optional[int] = 20,
        with_tag: Optional[bool] = True,
        with_label: Optional[bool] = True,
    ) -> Artifact:
        """Get an artifact.

        Parameters
        ----------
        project_name : str
            The name of the project
        repository_name : str
            The name of the repository
        reference : str
            The reference of the artifact, can be digest or tag
        page : int
            The page of results to return
        page_size : int
            The number of results to return per page
        with_tag : bool
            Whether to include the tags of the artifact in the response
        with_label : bool
            Whether to include the labels of the artifact in the response

        Returns
        -------
        Artifact
            An artifact.
        """
        artifact = await self.client.get_artifact(
            project_name,
            repository_name,
            reference,
            page=page,
            page_size=page_size,
            with_tag=with_tag,
            with_label=with_label,
        )
        return artifact

    async def copy_artifact(
        self, project_name: str, repository_name: str, source: str
    ) -> str:
        """Copy an artifact.

        Parameters
        ----------
        project_name : str
            Name of new artifact's project
        repository_name : str
            Name of new artifact's repository
        source : str
            The source artifact to copy from in the form of
            `"project/repository:tag"` or `"project/repository@digest"`

        Returns
        -------
        str
            The location of the new artifact
        """
        path = await self.client.copy_artifact(
            project_name, repository_name, source
        )
        return path

    async def delete_artifact(
        self,
        project_name: str,
        repository_name: str,
        reference: str,
    ) -> None:
        """Delete an artifact.

        Parameters
        ----------
        project_name : str
            The name of the project
        repository_name : str
            The name of the repository
        reference : str
            The reference of the artifact, can be digest or tag
        """
        await self.client.delete_artifact(
            project_name, repository_name, reference
        )

    async def get_artifact_vulnerabilities(
        self,
        project_name: str,
        repository_name: str,
        reference: str,
    ) -> HarborVulnerabilityReport:
        """Get the vulnerabilities for an artifact.

        Parameters
        ----------
        project_name : str
            The name of the project
        repository_name : str
            The name of the repository
        reference : str
            The reference of the artifact, can be digest or tag

        Returns
        -------
        HarborVulnerabilityReport
            The vulnerabilities for the artifact, or None if the artifact is not found   # NOQA: E501
        """
        report = await self.client.get_artifact_vulnerabilities(
            project_name,
            repository_name,
            reference,
        )
        return report

    async def get_artifact_tags(
        self,
        project_name: str,
        repository_name: str,
        reference: str,
        page: int = 1,
        page_size: int = 10,
        with_signature: bool = False,
        with_immutable_status: bool = False,
    ) -> List[Tag]:
        """Get the tags for an artifact.

        Parameters
        ----------
        project_name : str
            The name of the project
        repository_name : str
            The name of the repository
        reference : str
            The reference of the artifact, can be digest or tag
        page : int
            The page of results to return
        page_size : int
            The number of results to return per page
        with_signature : bool
            Whether to include the signature of the tag in the response
        with_immutable_status : bool
            Whether to include the immutable status of the tag in the response

        Returns
        -------
        List[Tag]
            A list of Tag objects for the artifact.
        """
        tags = await self.client.get_artifact_tags(
            project_name,
            repository_name,
            reference,
            page=page,
            page_size=page_size,
            with_signature=with_signature,
            with_immutable_status=with_immutable_status,
        )
        return tags

    async def create_artifact_tag(
        self, project_name: str, repository_name: str, reference: str, tag: Tag
    ) -> str:
        """Create a tag for an artifact.

        Parameters
        ----------
        project_name : str
            The name of the project
        repository_name : str
            The name of the repository
        reference : str
            The reference of the artifact, can be digest or tag
        tag : Tag
            The tag to create

        Returns
        -------
        str
            The location of the created tag
        """
        path = await self.client.create_artifact_tag(
            project_name, repository_name, reference, tag
        )
        return path

    async def delete_artifact_tag(
        self,
        project_name: str,
        repository_name: str,
        reference: str,
        tag_name: str,
    ) -> None:
        """Delete a tag for an artifact.

        Parameters
        ----------
        project_name : str
            The name of the project
        repository_name : str
            The name of the repository
        reference : str
            The reference of the artifact, can be digest or tag
        tag_name : str
            The name of the tag to delete
        """
        await self.client.delete_artifact_tag(
            project_name, repository_name, reference, tag_name
        )

    async def get_labels(
        self, name: Optional[str] = None, scope: Optional[str] = "g"
    ) -> List[Label]:
        """Get a list of labels.

        Parameters
        ----------
        name : Optional[str]
            The name of the label to filter by.
        scope : Optional[str]
            The scope of the label to filter by.
            Valid values are `"g"` and `"p"`.
            `"g"` for global labels and `"p"` for project labels.

        Returns
        -------
        List[Label]
            The list of labels.
        """
        labels = await self.client.get_labels(name=name, scope=scope)
        return labels

    async def create_label(self, label: str, scope: str = "g") -> str:
        """Create a label.

        Parameters
        ----------
        label : Label
            The label to create.
            The fields `id`, `creation_time` and `update_time` are ignored.

        Returns
        -------
        str
            The location of the label.
        """
        label = Label(name=label, scope=scope)
        path = await self.client.create_label(label)
        return path

    async def add_artifact_label(
        self,
        project_name: str,
        repository_name: str,
        reference: str,
        label: Label,
    ) -> None:
        """Add a label to an artifact.

        Parameters
        ----------
        project_name : str
            The name of the project
        repository_name : str
            The name of the repository
        reference : str
            The reference of the artifact, can be digest or tag
        label : Label
            The label to add
        """
        await self.client.add_artifact_label(
            project_name, repository_name, reference, label
        )

    async def delete_artifact_label(
        self,
        project_name: str,
        repository_name: str,
        reference: str,
        label_id: int,
    ) -> None:
        """Delete an artifact.

        Parameters
        ----------
        project_name : str
            The name of the project
        repository_name : str
            The name of the repository
        reference : str
            The reference of the artifact, can be digest or tag
        label_id : int
            The id of the label to delete
        """
        await self.client.delete_artifact_label(
            project_name, repository_name, reference, label_id
        )

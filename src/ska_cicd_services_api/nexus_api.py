# -*- coding: utf-8 -*-
import os
import urllib.parse
from typing import Any

import aiohttp


class NexusApi:
    """
    A class used to interact with Nexus Rest API v1

    For more information about the data returned by this class, please refer
    https://help.sonatype.com/repomanager3/rest-and-integration-api

    """

    def __init__(
        self,
        session: aiohttp.ClientSession,
        url: str = "https://artefact.skao.int",
        username: str = None,
        password: str = None,
    ):
        self._session = session
        # TODO: Parse and ensure valid URL
        self.base_url = os.getenv("NEXUS_URL", url)
        self.base_url = self.format_url("/service/rest/v1/")
        self._username = os.getenv("NEXUS_API_USERNAME", username)
        self._password = os.getenv("NEXUS_API_PASSWORD", password)
        if self._username is not None and self._password is not None:
            self._auth = aiohttp.BasicAuth(self._username, self._password)
        else:
            self._auth = None

    def format_url(self, url: str):
        url = urllib.parse.urljoin(self.base_url, url.lstrip("/"))
        return url

    async def get_components_list(self, repository: str):
        """Get nexus component information

        :return: https://help.sonatype.com/repomanager3/rest-and-integration-api/components-api#ComponentsAPI-ListComponents # NOQA: E501

        """
        url = "/components"
        params = {"repository": repository}
        return self._get_iter(url, params=params)

    async def get_component_info(self, component_id: str):
        """Get nexus component information

        :param: component_id: the id of the component

        :return: https://help.sonatype.com/repomanager3/rest-and-integration-api/components-api#ComponentsAPI-GetComponent # NOQA: E501

        """
        url = f"/components/{component_id}"
        return await self._request("GET", url)

    async def remove_component(self, component_id: str):
        """Remove a component from a nexus repository

        :param: component_id: the name of the artefact

        :return: https://help.sonatype.com/repomanager3/rest-and-integration-api/components-api#ComponentsAPI-DeleteComponent # NOQA: E501

        Note: Although Nexus API provides multiple uploads for raw and Maven types, it's not supported here at the moment.

        """
        url = f"/components/{component_id}"
        return await self._request("DELETE", url)

    async def upload_component(  # pylint: disable = too-many-arguments
        self,
        repository_type: str,
        repository: str,
        apt_asset=None,
        yum_directory: str = None,
        yum_asset=None,
        yum_asset_filename: str = None,
        rubygems_asset=None,
        nuget_asset=None,
        pypi_asset=None,
        helm_asset=None,
        npm_asset=None,
        raw_directory: str = None,
        raw_asset1=None,
        raw_asset1_filename: str = None,
        maven2_group_id: str = None,
        maven2_artifact_id: str = None,
        maven2_version: str = None,
        maven2_generate_pom: bool = False,
        maven2_packaging: str = None,
        maven2_asset1=None,
        maven2_asset1_classifier: str = None,
        maven2_asset1_extension: str = None,
    ):
        """Upload a component to the nexus repository

        :param: repository: the name of the nexus repository

        :return: https://help.sonatype.com/repomanager3/rest-and-integration-api/components-api#ComponentsAPI-UploadComponent # NOQA: E501

        """
        repository_types = {
            "maven": {
                "maven2.group.id": maven2_group_id,
                "maven2.artifact.id": maven2_artifact_id,
                "maven2.version": maven2_version,
                "maven2.generate-pom": maven2_generate_pom,
                "maven2.packaging": maven2_packaging,
                "maven2.asset1": maven2_asset1,
                "maven2.asset1.extension": maven2_asset1_extension,
                "maven2.asset1.classifier": maven2_asset1_classifier,
            },
            "raw": {
                "raw.directory": raw_directory,
                "raw.asset1": raw_asset1,
                "raw.asset1.filename": raw_asset1_filename,
            },
            "pypi": {"pypi.asset": pypi_asset},
            "rubygems": {"rubygems.asset": rubygems_asset},
            "nuget": {"nuget.asset": nuget_asset},
            "npm": {"npm.asset": npm_asset},
            "helm": {"helm.asset": helm_asset},
            "apt": {"apt.asset": apt_asset},
            "yum": {
                "yum.directoy": yum_directory,
                "yum.asset": yum_asset,
                "yum.asset.filename": yum_asset_filename,
            },
        }

        params = {
            "repository": repository,
        }
        repository_fields = repository_types[repository_type]
        url = "/components"
        return await self._request(
            "POST", url, params=params, data=repository_fields
        )

    async def _request(self, method: str, url: str, params=None, data=None):
        async with self._session.request(
            method,
            self.format_url(url),
            params=params,
            data=data,
            auth=self._auth,
        ) as response:
            if response.ok:
                return (
                    await response.json()
                    if response.status == 200
                    else {"result": "successfull"}
                )

            raise HTTPException(response.status, reason=response.reason)

    async def _get_iter(self, url: str, params=None):
        async with self._session.get(
            self.format_url(url), params=params, auth=self._auth
        ) as response:
            if response.ok:
                resp_json = (
                    await response.json() if response.status == 200 else {}
                )
            else:
                raise HTTPException(response.status, response.reason)
            next_url = resp_json["continuationToken"]
            data = resp_json["items"]
            for item in data:
                yield item

            while next_url is not None:
                next_params = {"continuationToken": next_url}
                async with self._session.get(
                    self.format_url(url),
                    params={**params, **next_params},
                    auth=self._auth,
                ) as response:
                    if response.ok:
                        resp_json = (
                            await response.json()
                            if response.status == 200
                            else {}
                        )
                    else:
                        raise HTTPException(response.status, response.reason)
                    next_url = resp_json["continuationToken"]

                    data = resp_json["items"]
                    for item in data:
                        yield item


class NexusException(Exception):
    """Base exception for this library."""


class HTTPException(NexusException):
    """A general exception to represent HTTP responses."""

    def __init__(self, status_code: int, reason: str, *args: Any) -> None:
        self.status_code = status_code
        if args:
            super().__init__(*args)
        else:
            super().__init__(reason)

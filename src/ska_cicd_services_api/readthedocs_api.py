import os
import urllib.parse
from typing import Any

import httpx


class ReadTheDocsApi:
    """
    A class used to interact with Readthedocs REST API v3

    For more information about the data returned by this class, please refer to
    https://docs.readthedocs.io/en/stable/api/index.html

    """

    def __init__(self, session: httpx.AsyncClient, token: str = ""):
        self._session = session
        self.token = os.getenv("RTD_TOKEN", token)
        self._headers = {"Authorization": f"token {self.token}"}
        self.base_url = "https://readthedocs.org/api/v3/"

    def format_url(self, url: str):
        url = urllib.parse.urljoin(self.base_url, url.lstrip("/"))
        return url

    async def get_subprojects_list(self, project_slug):
        """Get subprojects list.

        :param: project_slug: the parent project

        :return: https://docs.readthedocs.io/en/stable/api/v3.html#subprojects-listing # NOQA: E501

        """
        url = f"/projects/{project_slug}/subprojects/"
        response = await self._session.get(
            self.format_url(url), headers=self._headers
        )
        if response.status_code == 200:
            resp_json = response.json()
        else:
            raise HTTPException(response.status_code, response.reason_phrase)
        next_url = resp_json["next"]

        data = resp_json["results"]
        for item in data:
            yield item

        while next_url is not None:
            response = await self._session.get(
                self.format_url(next_url), headers=self._headers
            )
            if response.status_code == 200:
                resp_json = response.json()
            else:
                raise HTTPException(
                    response.status_code, response.reason_phrase
                )
            next_url = resp_json["next"]

            data = resp_json["results"]
            for item in data:
                yield item

    async def get_subproject_detail(self, project_slug, sub_project_slug):
        """Get subproject detail.

        :param: project_slug: the parent project

        :param: sub_project_slug: the sub project

        :return: https://docs.readthedocs.io/en/stable/api/v3.html#project-details # NOQA: E501

        """
        url = f"/projects/{project_slug}/subprojects/{sub_project_slug}/"
        response = await self._session.get(
            self.format_url(url), headers=self._headers
        )
        if response.status_code == 200:
            return response.json()

        raise HTTPException(
            response.status_code, reason=response.reason_phrase
        )


class ReadtheDocsException(Exception):
    """Base exception for this library."""


class HTTPException(ReadtheDocsException):
    """A general exception to represent HTTP responses."""

    def __init__(self, status_code: int, reason: str, *args: Any) -> None:
        self.status_code = status_code
        if args:
            super().__init__(*args)
        else:
            super().__init__(reason)

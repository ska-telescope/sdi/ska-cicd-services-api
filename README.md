# SKA CICD Services API

## How to use

Initialise makefile submodule:

```console
git submodule update --init --recursive
```

Please install all the requirements with your preferred python virtual environment with the following bash command:

```console
poetry shell
poetry update
```

Note: Depending on your environment you may need to update your 'setuptools' to install dependencies: 'python3 -m pip install -U setuptools'.

Run lint with the following command:

```console
make python-lint
```

Apply formatting rules with:

```console
make python-format
```


This project uses tox environments. Run tests with the following command:

```console
make python-test
```

## GitLab API

The module ska_cicd_services_api.gitlab provides a class named GitLabApi which enables following:

- Get authenticated user information
- Get git diffs of a commit in a project
- Add a comment to a merge request (MR)
- Remove a comment from a merge request (MR)
- Get MR information
- Modify MR information
- Get MR Approvals (Approver related settings)
- Modify MR Approval settings
- Get project level merge request approvals settings (Maintainer related settings)
- Modify project level merge request approvals settings
- Get all commits in an MR
- Get Project settings information
- Modify Project settings
- Get Project members
- Get list of merge request comments
- Get a single comment in an MR
- Modify a single comment in an MR
- Search a string in project
- Get file information in a branch
- Create merge requests
- Create branches
- Delete branches
- Trigger a Pipeline for Vulnerability Scanning
- Approve MR
- Unapprove MR
- Download artifacts from Pipeline Job
- Get job token scope
- Modify job token access

It is also needed to define the OS variables in a `PrivateRules.mak` file:

```console
PRIVATE_TOKEN = yourauthenticationtoken
GITLAB_API_REQUESTER = yourrequesteruser
```

### How to add a new API endpoint

1. Define the API method with a clear explanation in docstring following the rules:
   - Please include all the mandatory fields in the actual API documentation (_GitLab's API documentation_) as mandatory arguments (_no value assignment_) and include all the optional arguments with defaul values.
   - Please use plurals in the method name if you are returning a list of results instead of a single item.
   - Please use appropriate verbs in method names:*(the names are loosely mapped to the HTTP Methods in parantheses)*: `get(GET)`, `create(POST)`, `add(POST/PUT)`,`modify(PUT)`, `remove(DELETE)`. Please refrain with starting any other words. `add` may be used for some requests that make more sense contextually such as adding approvals to an MR instead of creating approvals.
   - If the method is returning a single item add `_info` postfix or plural form as explained above.
   - **Important:** if the API endpoint is returning a single item, you should use `await self.gl.getitem(url)` method.
   - **Important:** if the API endpoint is returning a list, you should use `self.gl.getiter(url)` method in order to ensure traversal of all results. Otherwise, it will only return first 20(default defined by GitLab API) results.
2. Write BDD tests for the API capability (relevant scenario and test fixtures)
3. Update the documentation in this README file

## Jira API

The module ska_cicd_services_api.jira provides a class named JiraApi which enables following:

- Get Project lists
- Get a single issue
- Create single/multiple issue(s)
- Add a comment to a given issue
- Delete a comment

Note that, the authenticated user must have necessary permissions for the above functionality to work correctly.

It is also needed to define the OS variables in a `PrivateRules.mak` file:

```console
JIRA_USERNAME = jirauser
JIRA_PASSWORD = strongpassword
JIRA_URL = https://jira.skatelescope.org
IGNORE_PROJECTS = IGP # list of project keys ignored
```

## ReadtheDocs API

The module ska_cicd_services_api.readthedocs provides a class named ReadTheDocsApi which enables following:

- Get subprojects of a project
- Get details of a single subproject with given parent project name

Note that, the authenticated user must have necessary permissions for the above functionality to work correctly.

It is also needed to define the OS variables in a `PrivateRules.mak` file:

```console
RTD_TOKEN = authtokenforreadthedocs
```

## Nexus API

The module ska_cicd_services_api.nexus provides a class named NexusApi which enables following:

- Get components lists given a nexus repository
- Get single component information given id
- Remove a component from its associated repository given id
- Upload a component to a nexus repository

Note that, the authenticated user must have necessary permissions for the above functionality to work correctly.

It is also needed to define the OS variables in a `PrivateRules.mak` file:

```console
NEXUS_API_USERNAME = jirauser
NEXUS_API_PASSWORD = strongpassword
NEXUS_URL = https://jira.skatelescope.org
```

## Harbor API

The module `ska_cicd_services_api.harbor` provides a class named `HarborApi` which enables the following:

- Get repository information
- Delete a repository
- Get a list of repositories
- Get artifact information
- Copy an artifact
- Delete an artifact
- Get artifact vulnerabilities
- Get artifact tags
- Create an artifact tag
- Delete an artifact tag
- Get a list of labels
- Create a label
- Add a label to an artifact
- Delete an artifact label

It is also needed to define the OS variables in a `PrivateRules.mak` file:

```makefile
HARBOR_URL = https://harbor.skatelescope.org
HARBOR_USERNAME = harboruser
HARBOR_PASSWORD = strongpassword
```

## People Database API

The module `ska_cicd_services_api.people_database` provides a class named `PeopleDatabaseApi` which enables the following:

- Get user Slack ID from GitLab handle
- Get user Slack ID from email
- Get user GitLab handle from email
- Get user email from GitLab handle

It is also needed to define the OS variables in a `PrivateRules.mak` file:

```makefile
PEOPLE_DB_SHEET_ID = yourspreadsheetid
PEOPLE_DB_SHEET_RANGE = yoursheetrange
PEOPLE_DB_NAME_COLUMN = 0
PEOPLE_DB_EMAIL_COLUMN = 1
PEOPLE_DB_SLACK_HANDLE_COLUMN = 2
PEOPLE_DB_GITLAB_HANDLE_COLUMN = 3
```

## Local Module Deployment and Testing

For local testing it might useful to install the modules with editable dependencies at the root (where `pyproject.toml` is located):

```console
poetry install
```

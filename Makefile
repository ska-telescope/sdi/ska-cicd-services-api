GITLAB_API_PRIVATE_TOKEN ?= ""
GITLAB_API_REQUESTER ?= ""
JIRA_PASSWORD ?= ""
JIRA_URL ?= "https://jira.skatelescope.org"
JIRA_USERNAME ?= ""
RTD_TOKEN ?= ""
HARBOR_URL ?= ""
HARBOR_USERNAME ?= ""
HARBOR_PASSWORD ?= ""


# include makefile targets for Kubernetes management
-include .make/python.mk

## The following should be standard includes
# include core makefile targets for release management
-include .make/base.mk

# include your own private variables for custom deployment configuration
-include PrivateRules.mak

PYTHON_VARS_BEFORE_PYTEST= \
		GITLAB_API_PRIVATE_TOKEN=$(GITLAB_API_PRIVATE_TOKEN) \
		GITLAB_API_REQUESTER=$(GITLAB_API_REQUESTER) \
		JIRA_PASSWORD=$(JIRA_PASSWORD) \
		JIRA_URL=$(JIRA_URL) \
		JIRA_USERNAME=$(JIRA_USERNAME) \
		RTD_TOKEN=$(RTD_TOKEN) \
		HARBOR_URL=$(HARBOR_URL) \
        HARBOR_USERNAME=$(HARBOR_USERNAME) \
        HARBOR_PASSWORD=$(HARBOR_PASSWORD) \
		PYTHONPATH=${PYTHONPATH}:.:./src \

PYTHON_SWITCHES_FOR_PYLINT = \
	--disable "fixme,duplicate-code,missing-module-docstring,missing-class-docstring,missing-function-docstring,line-too-long,too-many-lines,invalid-name,too-many-positional-arguments" \
	--min-public-methods 0 \
	--max-public-methods 40 \
	--max-args 10 \
	--max-attributes 10 \
	--max-locals 30 \


PYTHON_VARS_AFTER_PYTEST = -k 'not integration' --disable-pytest-warnings